service nginx start
service postgresql start
service ssh start
cd /home/kape

npm install
npm run build-production

pip install -r requirements.txt
python manage.py migrate
python manage.py collectstatic --noinput
python manage.py loaddata seeder.json

gunicorn --bind 0.0.0.0:8001 kape.wsgi:application --reload
