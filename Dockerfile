FROM node:4 AS frontend-builder

WORKDIR /home/kape/app
COPY . .
ARG NPM_PROXY=""
RUN npm config set proxy ${NPM_PROXY} \
    && npm install \
    && npm run build-production

FROM python:2-jessie AS app

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED 1

ARG USERNAME=kape
ARG USER_UID=1000
ARG USER_GID=$USER_UID

COPY requirements.txt /tmp/pip-tmp/

# Install system packages
RUN pip --disable-pip-version-check --no-cache-dir install -r /tmp/pip-tmp/requirements.txt \
    && rm -rf /tmp/pip-tmp

# Setup user
RUN groupadd --gid ${USER_GID} ${USERNAME} \
    && useradd -s /bin/bash --uid ${USER_UID} --gid ${USER_GID} -m ${USERNAME}

# Build the app
USER kape
WORKDIR /home/kape/app
COPY . .
COPY --chown=${USERNAME} --from=frontend-builder /home/kape/app/assets/bundles ./assets/bundles
COPY --chown=${USERNAME} --from=frontend-builder /home/kape/app/webpack-stats.json ./
RUN mkdir -p /home/kape/assets \
    && mkdir -p /home/kape/files \
    && python manage.py collectstatic --noinput

# Switch back to dialog for any ad-hoc use of apt-get
ENV DEBIAN_FRONTEND=

# Set environment variables
ENV KAPE_DB_NAME=kape
ENV KAPE_DB_USER=kape
ENV KAPE_DB_PASSWORD=kape
ENV KAPE_DB_HOST=db
ENV KAPE_DB_PORT=5432

# Run the app as non-root user
EXPOSE 8001
VOLUME ["/home/kape/assets", "/home/kape/files"]
CMD ["gunicorn", "--bind", "0.0.0.0:8001", "kape.wsgi:application", "--reload"]

# Container image metadata
## Note to editors: metadata values for `created`, `version`, and `revision`
## keys must be provided during build process, i.e. `docker build` invocation.
## It is also possible to pass other metadata values via build arguments.
ARG IMAGE_CREATED=""
ARG IMAGE_AUTHORS="PPLA1 Team & Faculty of Computer Science Universitas Indonesia"
ARG IMAGE_SOURCE="https://gitlab.cs.ui.ac.id/foss/kape"
ARG IMAGE_VERSION=""
ARG IMAGE_REVISION=""
ARG IMAGE_VENDOR="Faculty of Computer Science Universitas Indonesia"
ARG IMAGE_TITLE="Kape"
ARG IMAGE_DESCRIPTION="Fork of https://gitlab.com/PPL2017csui/PPLA1"
LABEL org.opencontainers.image.created=${IMAGE_CREATED} \
      org.opencontainers.image.authors=${IMAGE_AUTHORS} \
      org.opencontainers.image.source=${IMAGE_SOURCE} \
      org.opencontainers.image.version=${IMAGE_VERSION} \
      org.opencontainers.image.revision=${IMAGE_REVISION} \
      org.opencontainers.image.vendor=${IMAGE_VENDOR} \
      org.opencontainers.image.licenses="ISC" \
      org.opencontainers.image.title=${IMAGE_TITLE} \
      org.opencontainers.image.description=${IMAGE_DESCRIPTION}

## Note to editors: The following label assignments are to ensure backward
## compatibility with Label Schema standard
LABEL org.label-schema.build-date=${IMAGE_CREATED} \
      org.label-schema.vcs-url=${IMAGE_SOURCE} \
      org.label-schema.version=${IMAGE_VERSION} \
      org.label-schema.vendor=${IMAGE_VENDOR} \
      org.label-schema.title=${IMAGE_TITLE} \
      org.label-schema.description=${IMAGE_DESCRIPTION}