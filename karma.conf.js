var webpackConfig = require('./webpack.config');
webpackConfig.devtool = 'inline-source-map';

module.exports = function (config) {
  config.set({
    browsers: [ 'MyChrome' ], //  run in Firefox
    customLaunchers: {
      MyChrome: {
        base: 'Chrome',
        flags: ['--no-sandbox', '--disable-web-security', '--headless', '--disable-gpu', '--remote-debugging-port=9222']
      }
    },
    singleRun: false, //  just run once by default
    frameworks: [ 'mocha', 'chai' ], //  use the mocha test framework
    files: [
      'tests.webpack.js' //  just load this file
    ],
    plugins: [ 'karma-chrome-launcher', 'karma-mocha', 'karma-chai-plugins',
      'karma-sourcemap-loader', 'karma-webpack', 'karma-mocha-reporter', 'karma-coverage-istanbul-reporter'
    ],
    preprocessors: {
      'tests.webpack.js': [ 'webpack', 'sourcemap'] //  preprocess with webpack and our sourcemap loader
    },
    reporters: [ 'dots', 'mocha', 'coverage-istanbul' ], //  report results in this format
    webpack: webpackConfig,
    webpackServer: {
      noInfo: true //  please don't spam the console when running in karma!
    },
    coverageReporter: {
      type: 'html', //  produces a html document after code is run
      dir: 'test/frontend', //  path to created html doc
      subdir: '.',
    },
    coverageIstanbulReporter: {

      // reports can be any that are listed here: https://github.com/istanbuljs/istanbul-reports/tree/590e6b0089f67b723a1fdf57bc7ccc080ff189d7/lib
      reports: ['html', 'lcovonly', 'text-summary'],

      // base output directory. If you include %browser% in the path it will be replaced with the karma browser name
      dir: 'test/frontend', //path to created html doc

      // if using webpack and pre-loaders, work around webpack breaking the source path
      fixWebpackSourcePaths: true,

      // Most reporters accept additional config options. You can pass these through the `report-config` option
      'report-config': {

        // all options available at: https://github.com/istanbuljs/istanbul-reports/blob/590e6b0089f67b723a1fdf57bc7ccc080ff189d7/lib/html/index.js#L135-L137
        html: {
          // outputs the report in ./coverage/html
          subdir: 'html'
        }
        }
      }
  });
}