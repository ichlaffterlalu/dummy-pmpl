# __init__.py
from core.models.accounts import Company
from core.models.accounts import Student
from core.models.accounts import Supervisor
from core.models.vacancies import Vacancy
from core.models.vacancies import Application
from core.models.vacancies import VacancyMilestone
