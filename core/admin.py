from django.contrib import admin

from core.models.accounts import Company, Supervisor, Student
from core.models.feedbacks import Feedback
from core.models.vacancies import Vacancy

admin.site.register(Company)
admin.site.register(Student)
admin.site.register(Supervisor)
admin.site.register(Vacancy)
admin.site.register(Feedback)
