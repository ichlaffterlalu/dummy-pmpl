# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2019-10-05 15:56
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_merge_20191005_1957'),
        ('core', '0014_student_linkedin_url'),
    ]

    operations = [
    ]
