# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2019-10-05 08:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20170602_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='linkedin_url',
            field=models.URLField(blank=True, null=True),
        ),
    ]
