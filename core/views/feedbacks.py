from rest_framework import viewsets, status
from rest_framework.response import Response

from core.lib.permissions import IsAdminOrStudent
from core.models.feedbacks import Feedback
from core.serializers.feedbacks import FeedbackSerializer


class FeedbackViewSet(viewsets.GenericViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` feedback actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    permission_classes = [IsAdminOrStudent]

    def list(self, request):
        """
        Get list of feedbacks
        ---
        """
        feedbacks = Feedback.objects.all()
        page = self.paginate_queryset(feedbacks)
        serialized_page_data = FeedbackSerializer(page, many=True, context={'request': request}).data
        print (serialized_page_data)
        return self.get_paginated_response(serialized_page_data)

    def create(self, request):
        """
        Create a feedback
        ---
        """
        if 'title' in request.data and 'content' in request.data:
            new_feedback = Feedback(title=request.data['title'], content=request.data['content'])
            new_feedback.save()
            serialized_new_feedback = FeedbackSerializer(new_feedback, context={'request': request})
            serialized_new_feedback_data = serialized_new_feedback.data
            print(serialized_new_feedback_data)
            return Response(serialized_new_feedback_data)
        else:
            message = {"detail": "Please send a json object that have title and content property"}
            print(message)
            return Response(message, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        """
        Remove feedback {id}
        ---
        """
        if Feedback.objects.filter(id=pk).count() != 0:
            print (request.data)
            delete_feedback = Feedback.objects.get(id=pk)
            delete_feedback.delete()
            message = {"detail": "Feedback with id {} was removed".format(pk)}
            print(message)
            return Response(message, status=status.HTTP_200_OK)
        else:
            message = {"detail": "Feedback with id {} doesn't exist".format(pk)}
            print(message)
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
