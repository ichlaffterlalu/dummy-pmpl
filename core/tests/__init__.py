# __init__.py
from core.tests.test_accounts import LoginTests, RegisterTests
from core.tests.test_vacancies import ApplicationTests, BookmarkApplicationTests, CompanyListsTests
from core.tests.test_feedbacks import FeedbacksTests
