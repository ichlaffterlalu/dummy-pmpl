from datetime import datetime
from django.utils import timezone

import json
import requests_mock
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from core.models.accounts import Company, Student, Supervisor
from core.models.vacancies import Vacancy, Application, VacancyMilestone
from core.serializers.vacancies import VacancySerializer

class ApplicationTests(APITestCase):
    @requests_mock.Mocker()
    def test_application_list(self, m):
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'}, status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        url = '/api/students/' + str(student_id) + '/applied-vacancies/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @requests_mock.Mocker()
    def test_application_create_and_delete(self, m):
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'}, status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        new_user = User.objects.create_user('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)
        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())

        url = '/api/students/' + str(student_id) + '/applied-vacancies/'
        response = self.client.post(url, {'vacancy_id': new_vacancy.pk, 'cover_letter': 'this is a cover letter.'},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = '/api/students/' + str(student_id) + '/applied-vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class BookmarkApplicationTests(APITestCase):
    @requests_mock.Mocker()
    def test_application_list(self, m):
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'}, status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        url = '/api/students/' + str(student_id) + '/bookmarked-vacancies/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @requests_mock.Mocker()
    def test_application_create_and_delete(self, m):
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'}, status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        new_user = User.objects.create_user('dummy.company2', 'dummy.compan2y@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)
        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())

        url = '/api/students/' + str(student_id) + '/bookmarked-vacancies/'
        response = self.client.post(url, {'vacancy_id': new_vacancy.pk}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = '/api/students/' + str(student_id) + '/bookmarked-vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class VacancyTest(APITestCase):
    def test_verified_vacancy_list(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unverified_vacancy_list(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/?verified=false'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_filter_vacancy_list_by_company_ids(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        self.client.force_authenticate(user=superuser)

        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.companyzxc', 'dummy.companyzxc@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalalaasdsad", status=Company.VERIFIED,
                                              logo=None,
                                              address=None)
        open_time = datetime(2019, 10, 20)
        close_time = datetime(2019, 12, 20)
        vacancy1 = Vacancy.objects.create(company=new_company, verified=True, open_time=open_time,
                                            description='', close_time=close_time, name='vacancy1')
        vacancy2 = Vacancy.objects.create(company=new_company2, verified=True, open_time=open_time,
                                            description='', close_time=close_time, name='vacancy2')
        url = '/api/vacancies/?company={}&company={}'.format(new_company.id, new_company2.id)
        response = self.client.get(url, format='json')
        vacancies = Vacancy.objects.filter(company__id__in=[new_company.id, new_company2.id])
        self.assertEqual(dict(response.data)['count'], Vacancy.objects.count())
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_fail_on_unverified_user_vacancy_list(self):
        url = '/api/vacancies/'
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CompanyListsTests(APITestCase):
    def test_company_vacancy_list(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/vacancies/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_vacancy_list_unauthorized(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.companyzxc', 'dummy.companyzxc@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalalaasdsad", status=Company.VERIFIED,
                                              logo=None,
                                              address=None)

        self.client.force_authenticate(new_user2)

        url = '/api/companies/' + str(new_company.pk) + '/vacancies/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list(self):
        new_user = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_with_status(self):
        new_user = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/?status=0'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_unauthorized(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.companyzxc', 'dummy.companyzxc@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalalaasdsad", status=Company.VERIFIED,
                                              logo=None,
                                              address=None)

        self.client.force_authenticate(new_user2)

        url = '/api/companies/' + str(new_company.pk) + '/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list_by_vacancy(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_by_vacancy_unauthorized_1(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.student', 'dummy.company3@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        new_user3 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company3 = Company.objects.create(user=new_user3, description="lalala", status=Company.VERIFIED, logo=None,
                                              address=None)

        self.client.force_authenticate(new_user3)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list_by_vacancy_unauthorized_2(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.student', 'dummy.company3@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        new_user3 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company3 = Company.objects.create(user=new_user3, description="lalala", status=Company.VERIFIED, logo=None,
                                              address=None)

        self.client.force_authenticate(new_user3)

        url = '/api/companies/' + str(new_company3.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_company_application_list_by_vacancy_with_status(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/?status=2'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_company_application_list_by_vacancy_with_bad_status_string(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(
            new_vacancy.pk) + '/by_vacancy/?status=lalala'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_company_application_list_by_vacancy_with_bad_status_negative(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(
            new_vacancy.pk) + '/by_vacancy/?status=-1'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_company_application_list_by_vacancy_with_bad_status_large(self):
        new_user = User.objects.create_user('dummy.company3', 'dummy.company3@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user2, npm=1234123412)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())
        new_app = Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/' + str(new_vacancy.pk) + '/by_vacancy/?status=5'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_company_application_list_with_major(self):
        new_user = User.objects.create_user('dummy.company4', 'dummy.company4@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None, address=None)

        self.client.force_authenticate(new_user)

        url = '/api/companies/' + str(new_company.pk) + '/applications/?major=0'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class SupervisorStudentApplicationTests(APITestCase):
    def test_list_student_application(self):
        new_user = User.objects.create_user('dummy.supervisor', 'dummy.supervisor@asd.asd', 'lalala123')
        new_supervisor = Supervisor.objects.create(user=new_user, nip=1212121212)
        self.client.force_authenticate(user=new_user)

        url = '/api/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_student_application_unauthorized(self):
        new_user = User.objects.create_user('dummy.supervisor', 'dummy.supervisor@asd.asd', 'lalala123')
        self.client.force_authenticate(user=new_user)

        url = '/api/applications/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class SupervisorApprovalTests(APITestCase):
    def test_supervisor_approve_vacancy(self):
        new_user = User.objects.create_user('dummy.supervisor', 'dummy.supervisor@asd.asd', 'lalala123')
        new_supervisor = Supervisor.objects.create(user=new_user, nip=1212121212)
        self.client.force_authenticate(user=new_user)

        new_user2 = User.objects.create_user('dummy.company2', 'dummy.compan2y@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalala", status=Company.VERIFIED, logo=None,
                                              address=None)
        new_vacancy2 = Vacancy.objects.create(company=new_company2, verified=False, open_time=datetime.fromtimestamp(1541319300.0),
                                              description="lalala", close_time=timezone.now())

        url = '/api/vacancies/' + str(new_vacancy2.pk) + '/verify/'
        response = self.client.patch(url, {'verified': True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        retrieve_vacancy = Vacancy.objects.get(pk=new_vacancy2.pk)
        self.assertEqual(retrieve_vacancy.verified, True)

    def test_unauthorized_approve_vacancy(self):
        new_user = User.objects.create_user('dummy.companyz', 'dummy.companyz@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=new_user)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=False, open_time=datetime.fromtimestamp(1541319300.0),
                                             description="lalala", close_time=timezone.now())

        url = '/api/vacancies/' + str(new_vacancy.pk) + '/verify/'
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(new_vacancy.verified, False)


class VacancyMilestoneTests(APITestCase):
    def setUp(self):
        super(VacancyMilestoneTests, self).setUp()
        self.user = User.objects.create_user('dummy.student', 'dummy.student@home.com', 'lalala123')
        self.company_user =  User.objects.create_user('dummy.company2', 'dummy.compan2y@company.com', 'lalala123')
        self.company = Company.objects.create(user=self.company_user, description="lalala", status=Company.VERIFIED, logo=None,
                                              address=None)
        self.vacancy = Vacancy.objects.create(company=self.company, verified=True, open_time=datetime.fromtimestamp(0),
                                              description="lalala", close_time=datetime.today())

    def create_milestone_object(self):
        return VacancyMilestone.objects.create(vacancy=self.vacancy, name="initiate", detail="install things",
                                               expected_start=datetime.fromtimestamp(0),
                                               expected_finish=datetime.fromtimestamp(86400))

    def test_vacancy_milestone_model(self):
        milestone1 = VacancyMilestone(vacancy=self.vacancy, name="initiate", detail="install things",
                                      expected_start=datetime.fromtimestamp(0),
                                      expected_finish=datetime.fromtimestamp(86400))
        milestone1.full_clean()

        milestone2 = VacancyMilestone(vacancy=self.vacancy, name="a"*101, detail="install things",
                                      expected_start=datetime.fromtimestamp(0),
                                      expected_finish=datetime.fromtimestamp(86400))
        with self.assertRaises(ValidationError, msg="Name with more than 100 character should raise ValidationError"):
            milestone2.full_clean()

        milestone3 = VacancyMilestone(vacancy=self.vacancy, name="initiate", detail="install things",
                                      expected_start=datetime.fromtimestamp(86400),
                                      expected_finish=datetime.fromtimestamp(0))
        with self.assertRaises(ValidationError, msg="Expected finish earlier than tart should raise ValidationError"):
            milestone3.full_clean()

    def test_vacancy_milestones_list(self):
        milestone1 = self.create_milestone_object()
        self.client.force_authenticate(user=self.user)

        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["name"], milestone1.name)
        self.assertEqual(response.data["results"][0]["detail"], milestone1.detail)

    def test_create_new_milestone_on_a_vacancy_success(self):
        self.client.force_authenticate(user=self.company_user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/'
        data = {"name": "initiate", "detail": "install things", "expected_start": "2019-01-20",
                "expected_finish": "2019-01-21"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.vacancy.milestones.count(), 1)
        new_milestone = self.vacancy.milestones.first()
        self.assertEqual(new_milestone.name, data["name"])
        self.assertEqual(new_milestone.detail, data["detail"])

    def test_create_new_milestone_on_a_vacancy_invalid_data(self):
        self.client.force_authenticate(user=self.company_user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/'
        data = {"name": "initiate", "detail": "install things", "expected_start": "2019-01-20",
                "expected_finish": "2019-01-01"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(self.vacancy.milestones.count(), 0)

    def test_create_new_milestone_on_a_vacancy_unauthorized_user(self):
        self.client.force_authenticate(user=self.user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/'
        data = {"name": "initiate", "detail": "install things", "expected_start": "2019-01-20",
                "expected_finish": "2019-01-21"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(self.vacancy.milestones.count(), 0)

    def test_modify_milestone_on_a_vacancy_success(self):
        milestone = self.create_milestone_object()
        self.client.force_authenticate(user=self.company_user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/' + str(milestone.pk) + '/'
        data = {"name": "initiate env", "detail": "install all things", "expected_start": "2019-01-20",
                "expected_finish": "2019-01-21"}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.vacancy.milestones.count(), 1)
        new_milestone = self.vacancy.milestones.first()
        self.assertEqual(new_milestone.name, data["name"])
        self.assertEqual(new_milestone.detail, data["detail"])

    def test_modify_milestone_on_a_vacancy_not_found(self):
        milestone = self.create_milestone_object()
        self.client.force_authenticate(user=self.company_user)
        url = '/api/vacancies/1000/milestones/' + str(milestone.pk) + '/'
        data = {"name": "initiate env", "detail": "install all things", "expected_start": "2019-01-20",
                "expected_finish": "2019-01-21"}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        url2 = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/1000/'
        response2 = self.client.put(url2, data, format='json')
        self.assertEqual(response2.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(self.vacancy.milestones.count(), 1)
        new_milestone = self.vacancy.milestones.first()
        self.assertEqual(new_milestone.name, milestone.name)
        self.assertEqual(new_milestone.detail, milestone.detail)

    def test_modify_milestone_on_a_vacancy_invalid_data(self):
        milestone = self.create_milestone_object()
        self.client.force_authenticate(user=self.company_user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/' + str(milestone.pk) + '/'
        data = {"name": "a" * 101, "detail": "install all things", "expected_start": "2019-01-21",
                "expected_finish": "2019-01-19"}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(self.vacancy.milestones.count(), 1)
        new_milestone = self.vacancy.milestones.first()
        self.assertEqual(new_milestone.name, milestone.name)
        self.assertEqual(new_milestone.detail, milestone.detail)

    def test_modify_milestone_on_a_vacancy_unauthorized_user(self):
        milestone = self.create_milestone_object()
        self.client.force_authenticate(user=self.user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/' + str(milestone.pk) + '/'
        data = {"name": "initiate env", "detail": "install all things", "expected_start": "2019-01-20",
                "expected_finish": "2019-01-21"}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(self.vacancy.milestones.count(), 1)
        new_milestone = self.vacancy.milestones.first()
        self.assertEqual(new_milestone.name, milestone.name)
        self.assertEqual(new_milestone.detail, milestone.detail)

    def test_delete_milestone_on_a_vacancy_success(self):
        milestone = self.create_milestone_object()
        self.client.force_authenticate(user=self.company_user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/' + str(milestone.pk) + '/'
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.vacancy.milestones.count(), 0)

    def test_delete_milestone_on_a_vacancy_not_found(self):
        milestone = self.create_milestone_object()
        self.client.force_authenticate(user=self.company_user)
        url = '/api/vacancies/1000/milestones/' + str(milestone.pk) + '/'
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        url2 = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/1000/'
        response2 = self.client.delete(url2, format='json')
        self.assertEqual(response2.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(self.vacancy.milestones.count(), 1)

    def test_delete_milestone_on_a_vacancy_unauthorized_user(self):
        milestone = self.create_milestone_object()
        self.client.force_authenticate(user=self.user)
        url = '/api/vacancies/' + str(self.vacancy.pk) + '/milestones/' + str(milestone.pk) + '/'
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(self.vacancy.milestones.count(), 1)


class AcceptOneOfferTests(APITestCase):
    def generateObject(self):
        new_user = User.objects.create_user('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_user2 = User.objects.create_user('dummy.company2', 'dummy.company2@company.com', 'lalala123')
        new_company2 = Company.objects.create(user=new_user2, description="lalala", status=Company.VERIFIED, logo=None,
                                             address=None)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())

        new_vacancy2 = Vacancy.objects.create(company=new_company2, verified=True, open_time=datetime.fromtimestamp(0),
                                             description="lalala", close_time=datetime.today())

        new_user3 = User.objects.create_user('dummy.student', 'dummy.student@company.com', 'lalala123')
        new_student = Student.objects.create(user=new_user3, npm=1234123412)

        return new_user3, new_vacancy, new_vacancy2, new_student

    def test_number_of_content_response_object_given_id_auth(self):

        new_user3, new_vacancy, new_vacancy2, new_student = self.generateObject()

        self.client.force_authenticate(new_user3)

        Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")
        Application.objects.create(student=new_student, vacancy=new_vacancy2, cover_letter="asdasdasd")

        url = '/api/acceptoffer/' + str(new_student.pk) + '/vacancy/' + str(new_vacancy.pk) + '/'

        response = self.client.patch(url, format='json')
        body = json.loads(response.content)

        status_response = []
        for app in body:
            status_response.append(app['status'])

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(body) >= 2)
        self.assertFalse(len(body) == 0)
        self.assertTrue('new' in status_response)
        self.assertTrue('aborted' in status_response)

    def test_student_not_exist_given_auth(self):
        new_user3,new_vacancy, new_vacancy2, new_student = self.generateObject()

        self.client.force_authenticate(new_user3)

        user4 = User.objects.create_user('student_user_4', 'student_user_4@company.com', 'lalala123')
        other_student = Student.objects.create(user=user4, npm=1098765432)

        Application.objects.create(student=other_student, vacancy=new_vacancy, cover_letter="asdasdasd")
        Application.objects.create(student=other_student, vacancy=new_vacancy2, cover_letter="asdasdasd")

        url = '/api/acceptoffer/' + str(new_student.pk) + '/vacancy/' + str(new_vacancy.pk) + '/'

        response = self.client.patch(url, format='json')
        body = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(body) == 0)

    def test_type_error_if_input_null(self):
        new_user3, new_vacancy, new_vacancy2, new_student = self.generateObject()

        self.client.force_authenticate(new_user3)

        Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")
        Application.objects.create(student=new_student, vacancy=new_vacancy2, cover_letter="asdasdasd")

        with self.assertRaises(TypeError):
            url = '/api/acceptoffer/' + None + '/vacancy/' + str(new_vacancy.pk) + '/'

        with self.assertRaises(TypeError):
            url = '/api/acceptoffer/' + str(new_student.pk) + '/vacancy/' + None + '/'

    def test_if_requester_is_not_authenticated(self):
        new_user3, new_vacancy, new_vacancy2, new_student = self.generateObject()

        Application.objects.create(student=new_student, vacancy=new_vacancy, cover_letter="asdasdasd")
        Application.objects.create(student=new_student, vacancy=new_vacancy2, cover_letter="asdasdasd")

        url = '/api/acceptoffer/' + str(new_student.pk) + '/vacancy/' + str(new_vacancy.pk) + '/'

        response = self.client.patch(url, format='json')
        body = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
