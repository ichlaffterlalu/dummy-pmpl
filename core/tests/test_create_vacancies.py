from datetime import datetime

import requests_mock
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from core.models.accounts import Company, Student, Supervisor
from core.models.vacancies import Vacancy, Application

class CreateAndUpdateVacancyTest(APITestCase):
    def test_new_vacancy_success(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/'
        response = self.client.post(url, {'company': new_company.pk, 'open_time': datetime.fromtimestamp(0),
        	 'close_time': datetime.today(), 'name': 'new_vacancy', 'description': 'new_vacancy	'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        vacancies = Vacancy.objects.count()
        self.assertEqual(vacancies,1)

    def test_new_vacancy_with_amount_success(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/'
        response = self.client.post(url, {'company': new_company.pk, 'open_time': datetime.fromtimestamp(0),
                                          'close_time': datetime.today(), 'name': 'new_vacancy',
                                          'description': 'new_vacancy	', 'amount': 10}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        vacancy = Vacancy.objects.first()
        self.assertEqual(vacancy.amount, 10)

    def test_new_vacancy_failed(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/'
        response = self.client.post(url, {'company': new_company.pk, 'open_time': datetime.today(),
        	 'close_time': datetime.fromtimestamp(0), 'name': 'new_vacancy', 'description': 'new_vacancy'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        vacancies = Vacancy.objects.count()
        self.assertEqual(vacancies,0)

    def test_new_vacancy_with_amount_failed(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        url = '/api/vacancies/'
        response = self.client.post(url, {'company': new_company.pk, 'open_time': datetime.fromtimestamp(0),
                                          'close_time': datetime.today(), 'name': 'new_vacancy',
                                          'description': 'new_vacancy	', 'amount': 'sepuluh'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        vacancies_count = Vacancy.objects.count()
        self.assertEqual(vacancies_count, 0)

    def test_update_vacancy_success(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=False, open_time=datetime.fromtimestamp(0),
                                              description="lalala", close_time=datetime.today(), name='new_company')

        url = '/api/vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.patch(url, {'open_time': datetime.fromtimestamp(0), 'close_time': datetime.today(),
        	'name': 'new_vacancy2', 'description': 'new_vacancy2'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_vacancy_with_amount_success(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=False, open_time=datetime.fromtimestamp(0),
                                              description="lalala", close_time=datetime.today(), name='new_company')

        url = '/api/vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.patch(url, {'open_time': datetime.fromtimestamp(0), 'close_time': datetime.today(),
        	'name': 'new_vacancy2', 'description': 'new_vacancy2', 'amount': 10}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        vacancy = Vacancy.objects.first()
        self.assertEqual(vacancy.amount, 10)

    def test_update_vacancy_failed(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=False, open_time=datetime.fromtimestamp(0),
                                              description="lalala", close_time=datetime.today(), name='new_company')

        url = '/api/vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.patch(url, {'open_time': datetime.today(), 'close_time': datetime.fromtimestamp(0),
        	'name': 'new_vacancy2', 'description': 'new_vacancy2'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_vacancy_with_amount_failed(self):
        superuser = User.objects.create_superuser('dummy.company', 'dummy.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=superuser, description="lalalaz", status=Company.VERIFIED, logo=None,
                                             address=None)
        self.client.force_authenticate(user=superuser)

        new_vacancy = Vacancy.objects.create(company=new_company, verified=False, open_time=datetime.fromtimestamp(0),
                                              description="lalala", close_time=datetime.today(), name='new_company')

        url = '/api/vacancies/' + str(new_vacancy.pk) + '/'
        response = self.client.patch(url, {'open_time': datetime.fromtimestamp(0), 'close_time': datetime.today(),
        	'name': 'new_vacancy2', 'description': 'new_vacancy2', 'amount': 'sepuluh'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        vacancy = Vacancy.objects.first()
        self.assertEqual(vacancy.amount, None)
