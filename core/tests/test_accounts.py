import requests_mock
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth.models import User
from core.models.accounts import Company, Supervisor, Student


class LoginTests(APITestCase):
    @requests_mock.Mocker()
    def test_succesful_student_login_relogin(self, m):
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'}, status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
                "username": "dummy.mahasiswa",
                "nama": "Dummy Mahasiswa",
                "state": 1,
                "kode_org": "01.00.12.01:mahasiswa",
                "kodeidentitas": "1234567890",
                "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={
                "kota_lahir": "kota_kota",
                "tgl_lahir": "2017-12-31",
                "program": [{
                    "nm_org" : "Ilmu Informasi",
                    "angkatan" : "2017"
                }]
        }, status_code=200)

        url = '/api/login/'

        response = self.client.post(url, { 'username' : 'dummy.mahasiswa', 'password' : 'lalala', 'login-type' : 'sso-ui'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @requests_mock.Mocker()
    def test_successful_supervisor_login_relogin(self, m):
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'}, status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.dosen",
            "nama": "Dummy Dosen",
            "state": 1,
            "kode_org": "01.00.12.01:dosen",
            "kodeidentitas": "1234567891",
            "nama_role": "dosen"
        }, status_code=200)

        url = '/api/login/'
        response = self.client.post(url, {'username': 'dummy.dosen', 'password': 'lalala', 'login-type': 'sso-ui'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(url, {'username': 'dummy.dosen', 'password': 'lalala', 'login-type': 'sso-ui'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @requests_mock.Mocker()
    def test_failed_sso_login(self, m):
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "state": 0
        }, status_code=200)

        url = '/api/login/'
        response = self.client.post(url, {'username': 'dummy.salah', 'password': 'lalala', 'login-type': 'sso-ui'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_failed_company_login(self):
        url = '/api/login/'
        response = self.client.post(url, {'username': 'dummy.company.failed', 'password': 'lalala', 'login-type': 'company'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_success_company_login(self):
        new_user = User.objects.create_user('dummy.login.company', 'dummy.login.company@company.com', 'lalala123')
        new_company = Company.objects.create(user=new_user, description="lalalala", status=Company.VERIFIED, logo=None, address=None)

        url = '/api/login/'
        response = self.client.post(url, {'username': 'dummy.login.company', 'password': 'lalala123', 'login-type': 'company'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_bad_request_1(self):
        url = '/api/login/'
        response = self.client.post(url, {'username': 'lalala'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bad_request_2(self):
        url = '/api/login/'
        response = self.client.post(url, {'username': 'lalala', 'password': 'lalalala', 'login-type' : 'lalala'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class RegisterTests(APITestCase):

    def test_create_and_recreate(self):
        url = '/api/register/'
        tc_post = {'password': 'corporatepass', 'name':'tutuplapak', 'description':'menutup lapak', 'email': 'email@email.com', 'logo':'lalala', 'address':'alamat', 'category':'Perusahaan Jasa'}
        response = self.client.post(url, tc_post, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(url, tc_post, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)

    def test_bad_request(self):
        url = '/api/register/'
        response = self.client.post(url, {'username': 'lalala'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ProfileUpdateTests(APITestCase):

    @requests_mock.Mocker()
    def test_student_profile_update(self, m):
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'}, status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get('https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG', json={
            "kota_lahir": "kota_kota",
            "tgl_lahir": "2017-12-31",
            "program": [{
                "nm_org": "Ilmu Informasi",
                "angkatan": "2017"
            }]
        }, status_code=200)

        url = '/api/login/'
        response = self.client.post(url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                                    format='json')
        student_id = response.data.get('student').get('id')

        url = '/api/students/' + str(student_id) + "/profile/"
        response = self.client.patch(url, {'phone_number': '08123123123'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(response.data.get('phone_number'), '08123123123')

        url = '/api/students/' + str(student_id) + "/profile/"
        response = self.client.patch(url, {'linkedin_url': 'https://www.linkedin.com/in/jojo/'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(response.data.get('linkedin_url'), 'https://www.linkedin.com/in/jojo/')

        response = self.client.patch(url, {'region': 'Indonesia'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(response.data.get('region'), 'Indonesia')

        url = '/api/students/' + str(student_id) + "/profile/"
        response = self.client.patch(url, {'email': 'saasdasd'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        url = '/api/students/123123123/profile/'
        response = self.client.patch(url, {'phone_number': '08123123123'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        url = '/api/students/' + str(student_id) + "/profile/"
        response = self.client.patch(url, {'linkedin_url': 'this is not valid url'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        new_user = User.objects.create_user('dummy.student2', 'dummy.student@student.com', 'lalala123')
        new_student = Student.objects.create(user=new_user, npm="1212121212")

        url = '/api/students/' + str(new_student.pk) + "/profile/"
        response = self.client.patch(url, {'phone_number': '08123123123'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        url = '/api/students/' + str(student_id) + '/profile/'
        response = self.client.patch(url, {'intro': 'Saya tertarik dengan dunia front-end development'}, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(response.data.get('intro'), 'Saya tertarik dengan dunia front-end development')
