import requests_mock
from rest_framework import status
from rest_framework.test import APITestCase

from core.models.feedbacks import Feedback


class FeedbacksTests(APITestCase):
    def login(self, m):
        # Login
        m.get('https://akun.cs.ui.ac.id/oauth/token/verify/?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG',
              json={"username": 'dummy.mahasiswa', "role": 'mahasiswa', "identity_number": '1234567890'},
              status_code=200)
        m.post('https://api.cs.ui.ac.id/authentication/ldap/v2/', json={
            "username": "dummy.mahasiswa",
            "nama": "Dummy Mahasiswa",
            "state": 1,
            "kode_org": "01.00.12.01:mahasiswa",
            "kodeidentitas": "1234567890",
            "nama_role": "mahasiswa"
        }, status_code=200)
        m.get(
            'https://api.cs.ui.ac.id/siakngcs/mahasiswa/1234567890?client_id=X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG',
            json={
                "kota_lahir": "kota_kota",
                "tgl_lahir": "2017-12-31",
                "program": [{
                    "nm_org": "Ilmu Informasi",
                    "angkatan": "2017"
                }]
            }, status_code=200)
        login_url = '/api/login/'
        self.client.post(login_url, {'username': 'dummy.mahasiswa', 'password': 'lalala', 'login-type': 'sso-ui'},
                         format='json')

    @requests_mock.Mocker()
    def test_feedback_list_with_more_than_10_feedback_and_with_page_number(self, m):
        self.login(m)

        # test_feedback_list_with_more_than_10_feedback_and_with_page_number
        for i in range(15):
            Feedback.objects.create(content="a feedback", title="a title")

        feedbacks_url = '/api/feedbacks/'
        feedbacks_url_page_1 = '/api/feedbacks/?page=1'
        feedbacks_url_page_2 = '/api/feedbacks/?page=2'

        response1 = self.client.get(feedbacks_url_page_1)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data['count'], 15)
        self.assertIn(feedbacks_url_page_2, response1.data['next'])
        self.assertEqual(response1.data['previous'], None)
        self.assertEqual(len(response1.data['results']), 10)

        response2 = self.client.get(feedbacks_url_page_2)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.data['count'], 15)
        self.assertIn(feedbacks_url, response2.data['previous'])
        self.assertEqual(response2.data['next'], None)
        self.assertEqual(len(response2.data['results']), 5)

    @requests_mock.Mocker()
    def test_feedback_list_with_more_than_10_feedback_and_without_page_number(self, m):
        self.login(m)

        # test_feedback_list_with_more_than_10_feedback_and_without_page_number
        for i in range(15):
            Feedback.objects.create(content="a feedback", title="a title")

        feedbacks_url = '/api/feedbacks/'
        feedbacks_url_page_2 = '/api/feedbacks/?page=2'

        response1 = self.client.get(feedbacks_url)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data['count'], 15)
        self.assertIn(feedbacks_url_page_2, response1.data['next'])
        self.assertEqual(response1.data['previous'], None)
        self.assertEqual(len(response1.data['results']), 10)

        response2 = self.client.get(feedbacks_url_page_2)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.data['count'], 15)
        self.assertIn(feedbacks_url, response2.data['previous'])
        self.assertEqual(response2.data['next'], None)
        self.assertEqual(len(response2.data['results']), 5)

    @requests_mock.Mocker()
    def test_feedback_list_with_less_than_or_equals_10_feedbacks_and_with_zero_feedback_and_without_page_number(self,
                                                                                                                m):
        self.login(m)

        # test_feedback_list_with_more_than_10_feedback_and_without_page_number
        feedbacks_url = '/api/feedbacks/'

        response1 = self.client.get(feedbacks_url)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data['count'], 0)
        self.assertEqual(response1.data['next'], None)
        self.assertEqual(response1.data['previous'], None)
        self.assertEqual(len(response1.data['results']), 0)

    @requests_mock.Mocker()
    def test_feedback_list_with_less_than_or_equals_10_feedbacks_and_with_more_than_zero_feedback_and_with_page_number(
            self, m):
        self.login(m)

        # test_feedback_list_with_less_than_10_feedbacks_and_with_more_than_zero_feedback_and_with_page_number
        for i in range(5):
            Feedback.objects.create(content="a feedback", title="a title")

        feedbacks_url_page_1 = '/api/feedbacks/?page=1'

        response1 = self.client.get(feedbacks_url_page_1)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data['count'], 5)
        self.assertEqual(response1.data['previous'], None)
        self.assertEqual(response1.data['next'], None)
        self.assertEqual(len(response1.data['results']), 5)

    @requests_mock.Mocker()
    def test_feedback_list_with_less_than_or_equals_10_feedbacks_and_with_more_than_zero_feedback_and_without_page_number(
            self,
            m):
        self.login(m)

        # test_feedback_list_with_less_than_10_feedbacks_and_with_more_than_zero_feedback_and_without_page_number
        for i in range(5):
            Feedback.objects.create(content="a feedback", title="a title")

        feedbacks_url = '/api/feedbacks/'

        response1 = self.client.get(feedbacks_url)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data['count'], 5)
        self.assertEqual(response1.data['previous'], None)
        self.assertEqual(response1.data['next'], None)
        self.assertEqual(len(response1.data['results']), 5)

    @requests_mock.Mocker()
    def test_feedback_list_with_less_than_or_equals_10_feedbacks_and_with_zero_feedback_and_with_page_number(self, m):
        self.login(m)

        # test_feedback_list_with_less_than_10_feedbacks_and_with_zero_feedback_and_with_page_number
        feedbacks_url = '/api/feedbacks/?page=1'

        response1 = self.client.get(feedbacks_url)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data['count'], 0)
        self.assertEqual(response1.data['next'], None)
        self.assertEqual(response1.data['previous'], None)
        self.assertEqual(len(response1.data['results']), 0)

    @requests_mock.Mocker()
    def test_feedbacks_create_with_title_and_content(self, m):
        self.login(m)

        # test_feedbacks_create_with_title_and_content
        feedbacks_url = '/api/feedbacks/'
        response = self.client.post(feedbacks_url,
                                    {"content": "a content", "title": "a title"})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["title"], "a title")
        self.assertEqual(response.data["content"], "a content")

    @requests_mock.Mocker()
    def test_feedbacks_create_without_title_and_with_content(self, m):
        self.login(m)

        # test_feedbacks_create_without_title_and_with_content
        feedbacks_url = '/api/feedbacks/'
        response = self.client.post(feedbacks_url,
                                    {"content": "a content"})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], "Please send a json object that have title and content property")

    @requests_mock.Mocker()
    def test_feedbacks_create_without_content_and_with_title(self, m):
        self.login(m)

        # test_feedbacks_create_without_content_and_with_title
        feedbacks_url = '/api/feedbacks/'
        response = self.client.post(feedbacks_url,
                                    {"title": "a title"})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], "Please send a json object that have title and content property")

    @requests_mock.Mocker()
    def test_feedbacks_create_without_content_and_without_title(self, m):
        self.login(m)

        # test_feedbacks_create_without_content_and_without_title
        feedbacks_url = '/api/feedbacks/'
        response = self.client.post(feedbacks_url,
                                    {})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], "Please send a json object that have title and content property")

    @requests_mock.Mocker()
    def test_feedbacks_delete_with_exist_data_and_with_data_id(self, m):
        self.login(m)

        # test_feedbacks_delete_with_exist_data_and_with_data_id

        new_feedback = Feedback(title="a title", content="a content")
        new_feedback.save()
        feedback_id = new_feedback.id

        feedbacks_url = '/api/feedbacks/' + str(feedback_id) + '/'
        response = self.client.delete(feedbacks_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["detail"], "Feedback with id {} was removed".format(feedback_id))

    @requests_mock.Mocker()
    def test_feedbacks_delete_with_exist_data_and_with_data_id(self, m):
        self.login(m)

        # test_feedbacks_delete_with_exist_data_and_with_data_id

        new_feedback = Feedback(title="a title", content="a content")
        new_feedback.save()
        feedback_id = new_feedback.id

        feedbacks_url = '/api/feedbacks/' + str(feedback_id) + '/'
        response = self.client.delete(feedbacks_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["detail"], "Feedback with id {} was removed".format(feedback_id))

    @requests_mock.Mocker()
    def test_feedbacks_delete_with_not_exist_data_and_with_data_id(self, m):
        self.login(m)

        # test_feedbacks_delete_with_not_exist_data_and_with_data_id
        new_feedback = Feedback(title="a title", content="a content")
        new_feedback.save()
        feedback_id = new_feedback.id
        new_feedback.delete()

        feedbacks_url = '/api/feedbacks/' + str(feedback_id) + '/'
        response = self.client.delete(feedbacks_url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["detail"], "Feedback with id {} doesn't exist".format(feedback_id))

    @requests_mock.Mocker()
    def test_feedbacks_delete_without_data_id(self, m):
        self.login(m)

        # test_feedbacks_delete_without_data_id
        feedbacks_url = '/api/feedbacks/'
        response = self.client.delete(feedbacks_url)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
