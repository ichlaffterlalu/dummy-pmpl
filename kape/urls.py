"""djangoreact URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from core import apps
from core.views.accounts import StudentViewSet, CompanyViewSet, SupervisorViewSet, UserViewSet, LoginViewSet, \
    CompanyRegisterViewSet
from core.views.vacancies import VacancyViewSet, BookmarkedVacancyByStudentViewSet, StudentApplicationViewSet, \
    CompanyApplicationViewSet, CompanyVacanciesViewSet, ApplicationViewSet, VacancyMilestoneViewSet, \
    AcceptOfferByStudentViewSet
from core.views.feedbacks import FeedbackViewSet

schema_view = get_swagger_view()
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'students', StudentViewSet)
router.register(r'companies', CompanyViewSet)
router.register(r'supervisors', SupervisorViewSet)
router.register(r'login', LoginViewSet)
router.register(r'register', CompanyRegisterViewSet)
router.register(r'vacancies', VacancyViewSet)
router.register(r'applications', ApplicationViewSet)
router.register(r'feedbacks', FeedbackViewSet)
# router.register(r'students/(?P<student_id>\d+)/profile', StudentProfileViewSet)
router.register(r'vacancies/(?P<vacancy_id>\d+)/milestones', VacancyMilestoneViewSet,
                base_name='vacancy-milestones')
router.register(r'acceptoffer/(?P<student_id>\d+)/vacancy', AcceptOfferByStudentViewSet)
router.register(r'students/(?P<student_id>\d+)/bookmarked-vacancies', BookmarkedVacancyByStudentViewSet,
                base_name='bookmarked-vacancy-list')
router.register(r'students/(?P<student_id>\d+)/applied-vacancies', StudentApplicationViewSet,
                base_name='applications')
router.register(r'companies/(?P<company_id>\d+)/applications', CompanyApplicationViewSet,
                base_name='company-applications')
router.register(r'companies/(?P<company_id>\d+)/vacancies', CompanyVacanciesViewSet,
                base_name='company-vacancies')

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [
    url(r'^silk/', include('silk.urls', namespace='silk')),
    url(r'^api/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api$', schema_view),
    url(r"^api/", include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'', apps.index, name="index"),
]
