import React from 'react';
import { Button } from 'semantic-ui-react';
import Tabs from './components/Tabs';
import CompanyList from './components/CompanyList';
import Company from './components/Company';
import Pagination from './components/Pagination';

export default class CompanyPage extends React.Component {

  static propTypes = {
    user: React.PropTypes.object.isRequired,
  };

  handleClick = () => window.open('/admin/');

  render() {
    return (
      <div>
        <div className="administrationButtons">
          <Button onClick={this.handleClick} icon="dashboard" labelPosition="left" color="facebook" content="Buka Menu Administrasi" />
        </div>

        <Tabs selected={0}>
          <Pagination
            key={1}
            url={`/companies/?status=${Company.COMPANY_STATUS.NEW}`}
            label="Belum Diverifikasi"
            child={
              <CompanyList status={Company.COMPANY_STATUS.NEW} />
            }
          />
          <Pagination
            key={2}
            url={`/companies/?status=${Company.COMPANY_STATUS.VERIFIED}`}
            label="Terverifikasi"
            child={
              <CompanyList status={Company.COMPANY_STATUS.VERIFIED} />
            }
          />
          <Pagination
            key={3}
            url={`/companies/?status=${Company.COMPANY_STATUS.UNVERIFIED}`}
            label="Ditolak"
            child={
              <CompanyList status={Company.COMPANY_STATUS.UNVERIFIED} />
            }
          />
          <Pagination
            key={4}
            url={'/companies/'}
            label="Semua Perusahaan"
            child={
              <CompanyList status={Company.COMPANY_STATUS.ALL} />
            }
          />
        </Tabs>
      </div>
    );
  }
}
