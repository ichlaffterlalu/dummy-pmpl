/* eslint-disable no-console */

//  noinspection ES6ModulesDependencies,JSUnresolvedVariable
const DEV = process.env.NODE_ENV !== 'production';

export default class Logger {
  static log(...args) {
    if (DEV) {
      console.log(...args);
    }
    return args;
  }

  static warn(...args) {
    if (DEV) {
      console.warn(...args);
    }
    return args;
  }

  static error(...args) {
    if (DEV) {
      console.error(...args);
    }
    return args;
  }
}

global.Logger = Logger;
