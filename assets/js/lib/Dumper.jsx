/* eslint-disable no-console no-restricted-syntax no-param-reassign */

export default class Dumper {
  static dump(obj, indent) {
    let result = '';
    if (indent == null) indent = '';

    for (const property in obj) {
      let value = obj[property];

      if (typeof value === 'string') {
        value = `${value}`;
      } else if (typeof value === 'object') {
        if (value instanceof Array) {
          // Just let JS convert the Array to a string!
          value = `[ ${value} ]`;
        } else {
          const od = this.dump(value, `${indent}  `);
          value = `\n${od}`;
        }
      }
      result += `${indent}${property} : ${value},\n`;
    }
    return result.replace(/,\n$/, '');
  }
}
