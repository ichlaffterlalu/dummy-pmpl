import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import Tabs from './components/Tabs';
import ApplicantList from './components/ApplicantList';
import Applicant from './components/Applicant';
import Pagination from './components/Pagination';
import Server from './lib/Server';

export default class ApplicantPage extends React.Component {

  static propTypes = {
    user: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      email: '',
      password: '',
      errorFlag: false,
      company: { id: 1 },
      urls: [],
      selected: `/companies/${this.props.user.data.company.id}/applications/`,
      refresh: 0,
    };
    this.getVacancyList = this.getVacancyList.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getVacancyList();
  }


  getVacancyList= () => Server.get(`/companies/${this.props.user.data.company.id}/vacancies/`, false).then((data) => {
    const results = data.results;
    const names = ['Semua Lowongan'];
    const urls = [{
      key: 0,
      value: `/companies/${this.props.user.data.company.id}/applications/`,
      text: 'Semua Lowongan',
    }];
    results.map((vacancy) => {
      names.push(vacancy.name);
      const url = `/companies/${this.props.user.data.company.id}/applications/${vacancy.id}/by_vacancy/`;
      const info = {
        key: vacancy.id,
        value: url,
        text: vacancy.name,
      };
      urls.push(info);
      return urls;
    });
    this.setState({ urls });
  }, error => error.then((r) => {
    this.modalAlert.open('Gagal mendapatkan daftar lowongan', r.detail);
  }));

  handleChange = (e, data) => {
    this.setState({ selected: data.value, refresh: this.state.refresh + 5 });
  };

  render() {
    const company = this.props.user.data.company;
    return (
      <div>
        <div className="dropdownApplicant">
          <Dropdown placeholder="Semua Lowongan" search selection options={this.state.urls} onChange={this.handleChange} />
        </div>
        <Tabs selected={0}>
          <Pagination
            key={1 + this.state.refresh}
            url={`${this.state.selected}?status=${Applicant.APPLICATION_STATUS.NEW}`}
            label="Lamaran Baru"
            child={
              <ApplicantList companyId={company.id} status={Applicant.APPLICATION_STATUS.NEW} />
            }
          />
          <Pagination
            key={2 + this.state.refresh}
            url={`${this.state.selected}?status=${Applicant.APPLICATION_STATUS.READ}`}
            label="Lamaran Dibaca"
            child={
              <ApplicantList companyId={company.id} status={Applicant.APPLICATION_STATUS.READ} />
            }
          />
          <Pagination
            key={3 + this.state.refresh}
            url={`${this.state.selected}?status=${Applicant.APPLICATION_STATUS.BOOKMARKED}`}
            label="Lamaran Ditandai"
            child={
              <ApplicantList
                companyId={company.id}
                status={Applicant.APPLICATION_STATUS.BOOKMARKED}
              />
            }
          />
          <Pagination
            key={4 + this.state.refresh}
            url={`${this.state.selected}?status=${Applicant.APPLICATION_STATUS.ACCEPTED}`}
            label="Lamaran Diterima"
            child={
              <ApplicantList
                companyId={company.id}
                status={Applicant.APPLICATION_STATUS.ACCEPTED}
              />
            }
          />
          <Pagination
            key={5 + this.state.refresh}
            url={`${this.state.selected}?status=${Applicant.APPLICATION_STATUS.REJECTED}`}
            label="Lamaran Ditolak"
            child={
              <ApplicantList
                companyId={company.id}
                status={Applicant.APPLICATION_STATUS.REJECTED}
              />
            }
          />
        </Tabs>
      </div>
    );
  }
}
