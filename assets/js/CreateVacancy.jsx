import React from 'react';
import { Segment, Button, Form, Header, Icon, Input } from 'semantic-ui-react';
import { browserHistory } from 'react-router';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import CKEditor from 'react-ckeditor-wrapper';
import ModalAlert from './components/ModalAlert';
import Server from './lib/Server';

export default class CreateVacancy extends React.Component {

  static propTypes = {
    params: React.PropTypes.object.isRequired,
    user: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEditor = this.handleEditor.bind(this);
    this.setCloseTime = this.setCloseTime.bind(this);
    this.setOpenTime = this.setOpenTime.bind(this);

    this.state = {
      formLoading: false,
      loading: !!this.props.params.id,
      company: this.props.user.data.company,
      vacancyId: this.props.params.id,
      open_time: moment(),
      close_time: moment(),
      name: '',
      description: '',
    };

    if (this.state.vacancyId) {
      Server.get(`/vacancies/${this.state.vacancyId}/`).then((r) => {
        this.setState({
          description: r.description,
          name: r.name,
          open_time: moment(r.open_time),
          close_time: moment(r.close_time),
          loading: false,
        });
      });
    }
  }

  setOpenTime(date) {
    this.setState({ open_time: date });
  }

  setCloseTime(date) {
    this.setState({ close_time: date });
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleEditor(value) {
    this.setState({ description: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ formLoading: true });

    const data = {};
    data.name = this.state.name;
    data.description = this.state.description;
    data.open_time = this.state.open_time.format();
    data.close_time = this.state.close_time.format();
    if (!this.state.vacancyId) {
      data.company = this.state.company.id;
    }

    const url = this.state.vacancyId ? `/vacancies/${this.state.vacancyId}/` : '/vacancies/';
    const method = this.state.vacancyId ? 'PATCH' : 'POST';

    Server.sendRequest(url, method, data).then(() => {
      browserHistory.push('/lowongan');
    }, error => error.then((r) => {
      this.modalAlert.open('Gagal Membuat Lowongan', r.detail);
      this.setState({ formLoading: false });
    }));
  };

  render = () => (
    <div className="create-lowongan" >
      <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
      <Segment className="form-segment">
        <Header as="h2" icon textAlign="center">
          <Icon name="briefcase" circular />
          <Header.Content>
              Lowongan KP
          </Header.Content>
        </Header>
        <Form loading={this.state.formLoading} onSubmit={this.handleSubmit}>
          <Form.Field label="Posisi" name="name" control={Input} onChange={this.handleChange} value={this.state.name} required />
          <label htmlFor="description"> <b> Deskipsi Lowongan </b> </label>
          { !this.state.loading &&
            <CKEditor value={this.state.description} onChange={this.handleEditor} /> }
          <script>CKEDITOR.replace( 'description' );</script>
          <br />
          <Form.Group widths="equal">
            <Form.Field
              className="open-time-field"
              control={DatePicker}
              label="Waktu Buka Lowongan"
              selected={this.state.open_time}
              onChange={this.setOpenTime}
              required
            />
            <Form.Field
              className="close-time-field"
              control={DatePicker}
              label="Waktu Tutup Lowongan"
              selected={this.state.close_time}
              onChange={this.setCloseTime}
              required
            />
          </Form.Group>
          <Button type="submit" primary floated="right">Submit</Button>
        </Form>
      </Segment>
    </div>
  );
}
