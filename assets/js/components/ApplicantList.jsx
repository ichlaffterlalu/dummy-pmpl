import React from 'react';
import { Item, Grid, Container } from 'semantic-ui-react';
import Applicant from './Applicant';

export default class ApplicantList extends React.Component {

  static propTypes = {
    items: React.PropTypes.array,
    status: React.PropTypes.number.isRequired,
  };

  static defaultProps = {
    items: [],
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { applications: this.props.items };
    this.generateApplicants = this.generateApplicants.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
  }

  updateStatus(id, status) {
    const obj = [];
    this.state.applications.map((application) => {
      const clonedObj = {};
      Object.assign(clonedObj, application);
      if (application.id === id) clonedObj.status = status;
      return obj.push(clonedObj);
    });
    this.setState({ applications: obj });
  }

  generateApplicants() {
    if (this.state.applications.length == 0){
      return (
        <Item className="applicantItems">
          <Grid.Row>
            <Container textAlign="center">
              <p>Tidak ada pelamar</p><br />
            </Container>
          </Grid.Row>
        </Item>
      );
    }
    return this.state.applications.map(application =>
      application.status === this.props.status && (<Applicant
        key={application.id} data={application}
        updateStatus={this.updateStatus}
      />),
    );
  }

  render = () => (
    <Grid container doubling>
      <Item.Group relaxed style={{ width: '100%' }}>
        { this.generateApplicants() }
      </Item.Group>
    </Grid>
  );
}
