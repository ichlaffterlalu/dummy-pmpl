import React from 'react';
import { Item, Rating, Button, Grid } from 'semantic-ui-react';
import moment from 'moment';
import ApplyModal from './ApplyModal';
import Server from '../lib/Server';
import ConfirmationModal from './ConfirmationModal';
import ModalAlert from './ModalAlert';

const defaultImage = 'https://semantic-ui.com/images/wireframe/image.png';

export default class Vacancy extends React.Component {
  static propTypes = {
    user: React.PropTypes.object.isRequired,
    // studentId: React.PropTypes.number.isRequired,
    data: React.PropTypes.object.isRequired,
    bookmarked: React.PropTypes.number,
    status: React.PropTypes.number,
  };

  static defaultProps = {
    bookmarked: 0,
    status: 0,
  };

  static APPLICATION_STATUS_TEXT = ['Dikirim', 'Dibaca', 'Ditandai', 'Ditolak', 'Diterima'];

  static APPLICATION_STATUS = {
    NEW: 0,
    READ: 1,
    BOOKMARKED: 2,
    REJECTED: 3,
    ACCEPTED: 4,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      bookmarked: this.props.bookmarked,
      registeredStatus: this.props.status,
      deleteLoading: false,
    };
    moment.locale('id');
    this.bookmark = this.bookmark.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
    this.generateAction = this.generateAction.bind(this);
    this.openConfirmationModal = this.openConfirmationModal.bind(this);
    this.removeVacancyApplication = this.removeVacancyApplication.bind(this);
  }

  bookmark() {
    const data = { vacancy_id: this.props.data.id };
    if (this.state.bookmarked < 1) {
      Server.post(`/students/${this.props.user.data.student.id}/bookmarked-vacancies/`, data);
    } else {
      Server.delete(`/students/${this.props.user.data.student.id}/bookmarked-vacancies/${this.props.data.id}/`);
    }
    this.state.bookmarked = 1 - this.state.bookmarked;
  }

  updateStatus = (registeredStatus = null) => this.setState({ registeredStatus });

  removeVacancyApplication() {
    this.setState({ deleteLoading: true });
    Server.delete(`/students/${this.props.user.data.student.id}/applied-vacancies/${this.props.data.id}/`).then(() => {
      this.modalAlert.open('Pendaftaran Berhasil Dibatalkan', 'Pendaftaran anda berhasil dihapus dari sistem\n');
      this.setState({ registeredStatus: null, deleteLoading: false });
    }, () => {
      this.modalAlert.open('Permintaan Gagal', 'Maaf permintaan anda gagal diproses sistem. Harap ulangi pendaftaran atau hubungi administrator\n');
      this.setState({ deleteLoading: false });
    });
  }

  openConfirmationModal() {
    this.confirmationModal.open(
      'Batalkan Pendaftaran?',
      'Aksi ini tidak dapat direka ulang. Pastikan anda benar-benar ingin membatalkan pendaftaran',
      'trash',
      this.removeVacancyApplication,
    );
  }

  generateAction() {
    const applyModal = (
      <ApplyModal
        updateStatus={this.updateStatus}
        active={this.state.registeredStatus === null}
        data={{
          header: this.props.data.name,
          description: this.props.data.description,
          id: this.props.data.id,
        }}
        resume={this.props.user.data.student.resume}
        buttonTitle="Detail"
        studentId={this.props.user.data.student.id}
      />
    );

    const cancelButton = <Button loading={this.state.deleteLoading} floated="right" color="red" onClick={this.openConfirmationModal}>Batal</Button>;
    const rejectedButton =
      <Button floated="right" color="red" disabled>{Vacancy.APPLICATION_STATUS_TEXT[this.state.registeredStatus]}</Button>;
    const acceptedButton =
      <Button floated="right" color="blue" disabled>{Vacancy.APPLICATION_STATUS_TEXT[this.state.registeredStatus]}</Button>;

    if (this.state.registeredStatus == null) {
      return applyModal;
    } else if (this.state.registeredStatus === Vacancy.APPLICATION_STATUS.REJECTED) {
      return rejectedButton;
    } else if (this.state.registeredStatus === Vacancy.APPLICATION_STATUS.ACCEPTED) {
      return acceptedButton;
    }
    return cancelButton;
  }

  render() {
    return (
      <Item className="applicantItems">
        <ConfirmationModal ref={(modal) => { this.confirmationModal = modal; }} />
        <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
        <Item.Image size="small" src={this.props.data.company.logo ? this.props.data.company.logo : defaultImage} />
        <Item.Content verticalAlign="middle" style={{ wordWrap: 'break-word', width: '100%' }} >
          <Item.Extra>
            <Grid.Row>
              <Grid.Column floated="left">
                <h4>{ this.props.data.name }</h4>
                { this.props.data.company.name }<br />
                { this.props.data.company.address }<br /><br />
                <b>{`Ditutup ${moment(moment(this.props.data.close_time)).fromNow()}`}</b>
              </Grid.Column>
              <Grid.Column floated="right" >
                <Grid.Row textAlign="center">
                  <Rating icon="star" onRate={this.bookmark} size="massive" defaultRating={this.props.bookmarked} maxRating={1} />
                </Grid.Row>
                <Grid.Row>
                  { this.generateAction() }
                </Grid.Row>
              </Grid.Column>
            </Grid.Row>
          </Item.Extra>
        </Item.Content>
      </Item>
    );
  }
}
