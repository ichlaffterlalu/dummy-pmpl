import React from 'react';
import { Grid, Segment, Table, Header, Image, Container } from 'semantic-ui-react';
import Course from './Course';

export default class CourseList extends React.Component {

  static propTypes = {
    data: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { course: this.props.data.transcript };
  }


  generateCourse() {
    return this.state.course.map((course, index) => course.kelas ? (<Course
      key={index}
      grade={course.nilai}
      courseName={course.kelas.nm_mk_cl.nm_mk}
    />) : null);
  }

  render = () => (
    <Segment className="transkrip">
      <Grid>
        <Grid.Row>
          <Segment basic >
            <Image src="/assets/img/UI.png" size="tiny" floated="right" />
          </Segment>
          <Segment basic >
            <h3 style={{ lineHeight: '1%' }}>Universitas Indonesia </h3>
            <h3 style={{ lineHeight: '1%' }}>Fakultas Ilmu Komputer </h3>
            <h3 style={{ lineHeight: '1%' }}>Program Studi S1</h3>
          </Segment>
        </Grid.Row>
        <Container fluid>
          <hr />
        </Container>
        <Grid.Row columns={1}>
          <Grid.Column>
            <Container fluid textAlign="center">
              <h2> Riwayat Akademik Mahasiswa </h2>
            </Container>
            <h3>Nama : {this.props.data.name}</h3>
            <Table unstackable>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Mata Kuliah</Table.HeaderCell>
                  <Table.HeaderCell>Nilai</Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                { this.generateCourse() }
              </Table.Body>
            </Table>

          </Grid.Column>
        </Grid.Row>

      </Grid>
    </Segment>
  );
}
