import React from 'react';

export default class Tabs extends React.Component {
  static displayName = 'Tabs';

  static propTypes = {
    selected: React.PropTypes.number.isRequired,
    children: React.PropTypes.oneOfType([
      React.PropTypes.array,
      React.PropTypes.element,
    ]).isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected,
    };
  }

  shouldComponentUpdate = (nextProps, nextState) => (
      this.props !== nextProps || this.state !== nextState
  );

  handleClick = (index, event) => {
    event.preventDefault();
    this.setState({
      selected: index,
    });
  };

  _renderTitles = () => {
    function labels(child, index) {
      const activeClass = (this.state.selected === index ? 'active' : '');
      return (
        <li key={index}>
          <a className={activeClass} href="#" onClick={this.handleClick.bind(this, index)}>
            {child.props.label}
          </a>
        </li>
      );
    }
    return (
      <ul className="tabs__labels">
        {this.props.children.map(labels.bind(this))}
      </ul>
    );
  };

  _renderContent = () => (
    <div className="tabs__content">
      {this.props.children[this.state.selected]}
    </div>
    );


  render = () => (
    <div className="tabs">
      {this._renderTitles()}
      {this._renderContent()}
    </div>
    );
}
