import React from 'react';

export default class Pane extends React.Component {
  static displayName = 'Pane';

  static propTypes = {
    children: React.PropTypes.any.isRequired,
  };

  render = () => (
    <div>
      {this.props.children}
    </div>
    )
}
