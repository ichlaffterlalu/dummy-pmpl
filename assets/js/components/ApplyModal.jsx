import React from 'react';
import { Icon, Modal, Button, TextArea, Form, Message } from 'semantic-ui-react';
import ModalAlert from './../components/ModalAlert';
import Server from './../lib/Server';

export default class ApplyModal extends React.Component {
  static propTypes = {
    data: React.PropTypes.object.isRequired,
    active: React.PropTypes.bool.isRequired,
    buttonTitle: React.PropTypes.string.isRequired,
    resume: React.PropTypes.string,
    studentId: React.PropTypes.number.isRequired,
    updateStatus: React.PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      modalOpen: false,
      coverLetter: '',
      load: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleApply = this.handleApply.bind(this);
  }

  componentWillUpdate() {
    this.fixBody();
  }

  componentDidUpdate() {
    this.fixBody();
  }

  fixBody = () => {
    const anotherModal = document.getElementsByClassName('ui page modals').length;
    if (anotherModal > 0) document.body.classList.add('scrolling', 'dimmable', 'dimmed');
  };

  handleChange(event) {
    this.setState({ coverLetter: event.target.value });
  }

  handleOpen() {
    this.setState({ modalOpen: true });
  }

  handleClose = () => this.setState({
    modalOpen: false,
    load: false,
  });

  handleApply = () => {
    this.setState({ load: true });
    const requestData = { vacancy_id: this.props.data.id, cover_letter: this.state.coverLetter };
    Server.post(`/students/${this.props.studentId}/applied-vacancies/`, requestData).then(() => {
      this.modalAlert.open('Pendaftaran Berhasil', 'Pendaftaran anda berhasil direkam. Harap menunggu kabar selanjutnya dari pihak yang terkait\n', () => {
        this.handleClose();
        this.props.updateStatus('registered');
      });
    }, () => this.modalAlert.open('Pendaftaran Gagal', 'Maaf pendaftaran yang anda lakukan gagal. Harap ulangi pendaftaran atau hubungi administrator\n', this.handleClose),
    );
  };

  render() {
    return (
      <Modal
        trigger={<Button primary onClick={this.handleOpen} floated="right">{this.props.buttonTitle}</Button>}
        closeIcon="close"
        open={this.state.modalOpen}
        onClose={this.handleClose}
      >
        <Modal.Header>{this.props.data.header}</Modal.Header>
        <Modal.Content>
          <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
          <Modal.Description>
            <Modal.Header> <h3> Deskripsi Lowongan </h3></Modal.Header>
            { <div dangerouslySetInnerHTML={{ __html: this.props.data.description }} /> }
          </Modal.Description>
          {this.props.active && (
            <div className="coverLetter">
              <br />
              <div className="linkCV">
                { this.props.resume ? (<a href={this.props.resume} target="_blank" rel="noopener noreferrer"> Klik untuk lihat CV terakhirmu</a>)
                  : (
                    <Message
                      error
                      icon="warning sign"
                      header="CV Tidak Ditemukan"
                      content="Anda belum mengunggah CV. Harap ubah profil anda terlebih dahulu pada halaman Profil."
                    />)
                  }
              </div>
              <br />
              <div>
                <h5>Cover Letter </h5>
                <Form >
                  <TextArea placeholder="Tell us more" size="big" onChange={this.handleChange} />
                </Form>
              </div>
            </div>
           )}
        </Modal.Content>
        <Modal.Actions>
          <Button loading={this.state.load} color="blue" disabled={!this.props.active} onClick={this.handleApply}>
            { this.props.active ? 'Daftar' : 'Sudah Terdaftar' } <Icon name="right chevron" />
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}
