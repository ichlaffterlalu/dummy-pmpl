import React from 'react';
import { Form, Input, Button, Message, Image, Segment } from 'semantic-ui-react';
import { browserHistory } from 'react-router';
import Server from '../lib/Server';
import Storage from '../lib/Storage';

export default class LoginForm extends React.Component {

  static propTypes = {
    type: React.PropTypes.string.isRequired,
    imgSrc: React.PropTypes.string,
    header: React.PropTypes.string,
    usernameLabel: React.PropTypes.string,
  };

  static defaultProps = {
    imgSrc: '',
    imgSize: 'small',
    header: 'Login',
    usernameLabel: 'Username',
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { username: '', password: '', errorFlag: false, loading: false };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event, name) {
    this.setState({ [name]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = {
      'login-type': this.props.type,
      username: this.state.username,
      password: this.state.password,
    };
    this.setState({ loading: true });
    Server.post('/login/', data).then((response) => {
      Storage.set('user-data', response);
      browserHistory.push('/home');
    }, () => {
      this.setState({ errorFlag: true, loading: false });
    });
  }

  render = () => (

    <div className="formLogin" >

      <Segment.Group>
        <Segment>
          <Image src={`./assets/img/${this.props.imgSrc}`} verticalAlign="middle" /> <span>{ this.props.header }</span>
        </Segment>

        <Segment inverted className="header" >
          <Form
            loading={this.state.loading}
            onSubmit={e => this.handleSubmit(e)}
            error={this.state.errorFlag}
          >
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="id"> { this.props.usernameLabel } </label>
                <Input
                  type="text"
                  id="email"
                  icon="user"
                  iconPosition="left"
                  placeholder={this.props.usernameLabel}
                  onChange={e => this.handleChange(e, 'username')}
                  required
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="password"> Password </label>
                <Input
                  type="password"
                  id="password"
                  icon="key"
                  iconPosition="left"
                  placeholder="password"
                  onChange={e => this.handleChange(e, 'password')}
                  required
                />
              </Form.Field>
            </Form.Group>
            <Button type="submit" fluid color="blue">Login</Button>
            <Message
              error
              content="Login gagal: username atau password salah."
            />
          </Form>
        </Segment>
      </Segment.Group>
    </div>
  )
}
