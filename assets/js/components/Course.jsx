import React from 'react';
import { Table } from 'semantic-ui-react';

export default class Course extends React.Component {
  static propTypes = {
    courseName: React.PropTypes.string.isRequired,
    grade: React.PropTypes.string.isRequired,
  };

  render = () => (
    <Table.Row>
      <Table.Cell>{this.props.courseName}</Table.Cell>
      <Table.Cell>{this.props.grade}</Table.Cell>
    </Table.Row>
  );
}
