import React from 'react';
import { Item, Grid, Button } from 'semantic-ui-react';
import VerifyAdminModal from './VerifyAdminModal';
import Server from '../lib/Server';

export default class AdminVacancy extends React.Component {
  static propTypes = {
    data: React.PropTypes.object.isRequired,
    updateStatus: React.PropTypes.func.isRequired,
  };

  changeVerifiedStatus() {
    let data = {};
    if (this.props.data.verified) {
      data = { verified: false };
    } else {
      data = { verified: true };
    }
    Server.patch(`/vacancies/${this.props.data.id}/verify/`, data).then((status) => {
      this.props.updateStatus(this.props.data.id, status.status);
    });
  }

  generateButton() {
    const unverifyButton = <Button floated="right" color="red" onClick={this.changeVerifiedStatus.bind(this)}>Batalkan Verifikasi</Button>;
    const verifyButton = <Button floated="right" color="blue" onClick={this.changeVerifiedStatus.bind(this)}>Verifikasi</Button>;

    if (this.props.data.verified) {
      return unverifyButton;
    }
    return verifyButton;
  }

  render() {
    const defaultImage = 'https://semantic-ui.com/images/wireframe/image.png';
    return (
      <Item className="adminItems">
        <Item.Image src={this.props.data.company.logo ? this.props.data.company.logo : defaultImage} size="small" />
        <Item.Content>
          <Item.Header as="a">{this.props.data.name}</Item.Header>
          <Grid.Row>
            <Grid.Column floated="left">
              <h4>{this.props.data.company.name} </h4>
              {this.props.data.company.address}
            </Grid.Column>
            <Grid.Column floated="right">
              {this.generateButton()}
            </Grid.Column>
          </Grid.Row>
        </Item.Content>
      </Item>
    );
  }
}
