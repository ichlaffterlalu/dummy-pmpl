import React from 'react';
import { Button, Header, Modal } from 'semantic-ui-react';

export default class VerifyAdminModal extends React.Component {

  state = { modalOpen: false }

  componentWillUpdate() {
    this.fixBody();
  }

  componentDidUpdate() {
    this.fixBody();
  }

  fixBody = () => {
    const anotherModal = document.getElementsByClassName('ui page modals').length;
    if (anotherModal > 0) document.body.classList.add('scrolling', 'dimmable', 'dimmed');
  };

  handleOpen = () => this.setState({
    modalOpen: true,
  });

  handleClose = () => this.setState({
    modalOpen: false,
  });

  render() {
    return (

      <Modal
        trigger={
          <Button color="blue" icon="right chevron" labelPosition="right" floated="right" content="ubah" onClick={this.handleOpen} />
        }
        closeIcon="close"
        open={this.state.modalOpen}
        onClose={this.handleClose}
      >
        <Modal.Header>Software Engineer</Modal.Header>
        <Modal.Content >
          <Modal.Description>
            <Header>Deskripsi Lowongan</Header>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit,
              sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
              laboris nisi ut aliquip ex ea commodo consequat.
          </Modal.Description>
        </Modal.Content>

        <Modal.Actions>
          <Button color="green" floated="right" onClick={this.handleClose}>Verifikasi</Button>
        </Modal.Actions>

      </Modal>
    );
  }
}
