import React from 'react';
import { Item, Grid, Container } from 'semantic-ui-react';
import Vacancy from './Vacancy';
import CompanyVacancy from './CompanyVacancy';
import AdminVacancy from './AdminVacancy';
import Server from '../lib/Server';

export default class VacancyList extends React.Component {

  static propTypes = {
    user: React.PropTypes.object.isRequired,
    userId: React.PropTypes.number.isRequired,
    items: React.PropTypes.array,
  };

  static defaultProps = {
    items: [],
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      vacancies: this.props.items,
      bookmarkList: [],
      appliedList: [],
      loading: true,
    };
    this.generateVacancies = this.generateVacancies.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
  }

  updateStatus(id, status) {
    const obj = [];
    this.state.vacancies.map((vacancy) => {
      if (vacancy.id !== id) return obj.push(vacancy);
      return null;
    });
    this.setState({ vacancies: obj });
  }

  deleteVacancy = id => Server.delete(`/vacancies/${id}/`, this.state).then(() => {
    this.modalAlert.open('Hapus Lowongan', 'Lowongan berhasil dihapus');
    const newVacancies = [];
    this.state.vacancies.map(vacancy => vacancy.id !== id && newVacancies.push(vacancy));
    this.setState({ vacancies: newVacancies });
  }, error => error.then((r) => {
    this.modalAlert.open('Gagal Menghapus Lowongan', r.error);
  }));

  generateVacancies() {
    if (this.state.vacancies.length === 0) {
      return (
        <Item className="vacancyItems">
          <Grid.Row>
            <Container textAlign="center">
              <p>Tidak ada</p><br />
            </Container>
          </Grid.Row>
        </Item>
      );
    }
    if (this.props.user.role === 'student') {
      return this.state.vacancies.map(vacancy =>
        (
          <Vacancy
            key={vacancy.id}
            status={vacancy.status}
            user={this.props.user}
            bookmarked={vacancy.bookmarked ? 1 : 0}
            data={vacancy}
            studentId={this.props.userId}
          />
        ),
      );
    } else if ((this.props.user.role === 'admin' && this.props.user.data.company != null)
      || this.props.user.role === 'company') {
      return this.state.vacancies.map(vacancy => (
        <Item.Group key={vacancy.id} relaxed style={{ width: '100%' }}>
          <CompanyVacancy
            key={vacancy.id}
            data={vacancy}
            deleteCallback={() => this.deleteVacancy(vacancy.id)}
          />
        </Item.Group>
      ),
      );
    }

    return this.state.vacancies.map(vacancy => (
      <AdminVacancy
        key={vacancy.id}
        data={vacancy}
        updateStatus={this.updateStatus}
      />),
    );
  }

  render = () => (
    <div>
      <Grid container columns="eleven" doubling style={{ display: 'block' }}>
        <Item.Group relaxed style={{ width: '100%' }}>
          { this.generateVacancies() }
        </Item.Group>
      </Grid>
    </div>
  );
}
