import React from 'react';
import { Item, Grid, Container } from 'semantic-ui-react';
import Company from '../components/Company';

export default class CompanyList extends React.Component {

  static propTypes = {
    items: React.PropTypes.array,
    status: React.PropTypes.number.isRequired,
  };

  static defaultProps = {
    items: [],
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { companies: this.props.items };

    this.generateCompanies = this.generateCompanies.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
  }

  updateStatus(id, status) {
    const obj = [];
    this.state.companies.map((company) => {
      const clonedObj = {};
      Object.assign(clonedObj, company);
      if (company.id === id) clonedObj.status = status;
      return obj.push(clonedObj);
    });
    this.setState({ companies: obj });
  }

  generateCompanies() {
    if (this.state.companies.length === 0) {
      return (
        <Item className="vacancyItems">
          <Grid.Row>
            <Container textAlign="center">
              <p>Tidak ada</p><br />
            </Container>
          </Grid.Row>
        </Item>
      );
    }
    return this.state.companies.map(company =>
      (company.status === this.props.status || Company.COMPANY_STATUS.ALL === this.props.status) &&
      (<Company
        key={company.id} data={company}
        updateStatus={this.updateStatus}
      />),
    );
  }

  render = () => (
    <div>
      <Grid container doubling>
        <Item.Group relaxed style={{ width: '100%' }}>
          { this.generateCompanies() }
        </Item.Group>
      </Grid>
    </div>
  );
}
