import React from 'react';
import { Modal, Button, Icon, Header } from 'semantic-ui-react';

export default class ConfirmationModal extends React.Component {

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      modalOpen: false,
      header: '',
      content: '',
      icon: 'trash',
      callback: () => {},
    };
    this.open = this.open.bind(this);
    this.handleYes = this.handleYes.bind(this);
  }

  componentWillUpdate() {
    this.fixBody();
  }

  componentDidUpdate() {
    this.fixBody();
  }

  fixBody = () => {
    const anotherModal = document.getElementsByClassName('ui page modals').length;
    if (anotherModal > 0) document.body.classList.add('scrolling', 'dimmable', 'dimmed');
  };

  handleClose = () => this.setState({
    modalOpen: false,
  });

  handleYes = () => { this.state.callback(); this.handleClose(); }

  open = (header = this.state.header, content = this.state.content, icon = this.state.icon, callback = this.state.callback()) => {
    this.setState({ modalOpen: true, header, content, callback, icon });
  };

  render = () => (
    <Modal
      basic size="small" open={this.state.modalOpen}
      onOpen={this.open}
    >
      <Header icon={this.state.icon} content={this.state.header} />
      <Modal.Content>
        <p>{this.state.content}</p>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={this.handleClose} basic color="red" inverted>
          <Icon name="remove" /> Tidak
        </Button>
        <Button onClick={this.handleYes} basic color="green" inverted>
          <Icon name="checkmark" /> Ya
        </Button>
      </Modal.Actions>
    </Modal>
    );
}
