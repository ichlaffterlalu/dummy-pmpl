import React from 'react';
import { Menu, Container, Icon, Loader } from 'semantic-ui-react';
import Server from '../lib/Server';
import ModalAlert from '../components/ModalAlert';


export default class Pagination extends React.Component {

  static propTypes = {
    url: React.PropTypes.string.isRequired,
    child: React.PropTypes.node.isRequired,
    error: React.PropTypes.string,
  };

  static defaultProps = {
    error: 'Gagal Mengambil Data',
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = {
      items: [],
      current: 1,
      next: '',
      prev: '',
      url: this.props.url,
      loading: true,
      dir: 0,
      start: true,
      finish: false,
    };
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
    this.getItemsData = this.getItemsData.bind(this);
    this.handleMovement = this.handleMovement.bind(this);
    this.refresh = this.refresh.bind(this);
    this.content = this.content.bind(this);
    this.pageMenu = this.pageMenu.bind(this);
    this.getItemsData();
  }

  getItemsData = () => Server.get(this.state.url, false).then((data) => {
    this.setState({ current: this.state.current + this.state.dir });
    this.setState(
      { items: data.results,
        next: `${this.props.url}?page=${this.state.current + 1}`,
        prev: `${this.props.url}?page=${this.state.current - 1}`,
        loading: false,
      });
    let first = true;
    let last = true;
    if (data.previous) {
      first = false;
    }
    if (data.next) {
      last = false;
    }
    this.setState({ first, last });
  }, error => error.then((r) => {
    this.modalAlert.open(this.props.error, r.detail);
    this.setState({ loading: false });
  }));

  refresh() {
    this.forceUpdate();
  }

  handleMovement(dir) {
    const newUrl = this.state[dir];
    this.setState({ url: newUrl, loading: true }, function () {
      this.getItemsData();
    });
  }

  handlePrev() {
    if (!this.state.first) {
      this.setState({ dir: -1 }, function () {
        this.handleMovement('prev');
      });
    }
  }

  handleNext = () => {
    if (!this.state.last) {
      this.setState({ dir: 1 }, function () {
        this.handleMovement('next');
      });
    }
  };

  pageMenu() {
    return (<Container textAlign="right">
      <Menu pagination icon="labeled" className="vacancyList">
        <Menu.Item name="prev" disabled={this.state.first} onClick={this.handlePrev}>
          <span><Icon disabled={this.state.first} name="angle left" /></span>
        </Menu.Item>
        <Menu.Item name="current" active onClick={this.refresh}>
          {this.state.current}
        </Menu.Item>
        <Menu.Item name="next" disabled={this.state.last} onClick={this.handleNext}>
          <span><Icon disabled={this.state.last} name="angle right" /></span>
        </Menu.Item>
      </Menu>
    </Container>);
  }

  content() {
    return React.cloneElement(this.props.child, { items: this.state.items });
  }

  render = () => (
    <div>
      <Loader active={this.state.loading} />
      <ModalAlert ref={(modal) => { this.modalAlert = modal; }} />
      {!this.state.loading && this.content()}
      {!this.state.loading && this.pageMenu()}
    </div>
  );
}
