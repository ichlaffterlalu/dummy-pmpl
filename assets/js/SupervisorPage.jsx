import React from 'react';
import { Header, Icon, Grid } from 'semantic-ui-react';
import Pagination from './components/Pagination';
import Server from './lib/Server';
import ApplicationList from './components/ApplicationList';

const cols = [
    { key: 'StudentName', label: 'Nama' },
    { key: 'StudentID', label: 'NPM' },
	{ key: 'Major', label: 'Major' },
    { key: 'Perusahaan', label: 'Perusahaan' },
    { key: 'Posisi', label: 'Posisi' },
    { key: 'Status', label: 'Status' },
];

export default class SupervisorPage extends React.Component {

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { list: [] };
  }

  componentDidMount() {
    this.UserList();
  }

  UserList() {
    Server.get('/applications/', false).then((data) => {
      this.setState({ list: data.results });
    });
  }

  render = () => {
    return (
      <Grid container columns="eleven" doubling>
        <Grid.Row>
          <br />
        </Grid.Row>
        <Grid.Row>
          <Header as="h2">
            <Icon name="list" />
            Daftar Mahasiswa
          </Header>
        </Grid.Row>
        <Grid.Row>
          <div id="layout-content" className="layout-content-wrapper">
            <Pagination url={'/applications/'} child={<ApplicationList cols={cols} />} />
          </div>
        </Grid.Row>
      </Grid>
    );
  }
}
