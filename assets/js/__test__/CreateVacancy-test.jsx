import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import moment from 'moment';
import CreateVacancy from '../CreateVacancy';
import Storage from '../lib/Storage';

describe('CreateVacancy', () => {
  const companySession = {
    url: 'http://localhost:8001/api/users/8/',
    username: 'Tutuplapak',
    email: '',
    is_staff: false,
    company: {
      id: 3,
      user: {
        url: 'http://localhost:8001/api/users/8/',
        username: 'Tutuplapak',
        email: '',
        is_staff: false,
      },
      name: 'Tutuplapak',
      created: '2017-03-28T07:30:10.535000Z',
      updated: '2017-03-28T07:30:10.535000Z',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
      verified: true,
      logo: 'http://localhost:8001/files/company-logo/8a258a48-3bce-4873-b5d1-538b360d0059.png',
      address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
    },
    supervisor: null,
    student: null,
  };

  const errorSession = {
    url: 'http://localhost:8001/api/users/8/',
    username: 'Tutuplapak',
    email: '',
    is_staff: false,
    company: null,
    supervisor: null,
    student: null,
  };

  it('renders for companies without problem', () => {
    fetchMock.get('*', {});
    Storage.set('user-data', companySession);
    const createVacancy = ReactTestUtils.renderIntoDocument(
      <CreateVacancy params={{ id: 1 }} user={{ data: { company: {} } }} />,
    );
    expect(createVacancy).to.exist;
    fetchMock.restore();
  });


  it('renders without problem for error case', () => {
    fetchMock.get('*', {});
    Storage.set('user-data', errorSession);
    const createVacancy = ReactTestUtils.renderIntoDocument(
      <CreateVacancy params={{ id: 1 }} user={{ data: { company: {} } }} />,
    );
    expect(createVacancy).to.exist;
    fetchMock.restore();
  });

  it('support handle change', () => {
    fetchMock.get('*', {});
    Storage.set('user-data', companySession);
    const createVacancy = ReactTestUtils.renderIntoDocument(
      <CreateVacancy params={{ id: 1 }} user={{ data: { company: {} } }} />,
    );
    createVacancy.setState({
      name: 'stub',
      description: 'stub',
    });
    createVacancy.handleChange({ target: { name: 'test', value: 'hue' } });
    expect(createVacancy.state.test).to.equal('hue');
    fetchMock.restore();
  });

  it('submit vacancy properly (loading)', () => {
    fetchMock.post('*', 404);
    fetchMock.get('*', {});
    Storage.set('user-data', companySession);
    const createVacancy = ReactTestUtils.renderIntoDocument(
      <CreateVacancy params={{ id: undefined }} user={{ data: { company: {} } }} />,
    );
    createVacancy.setState({
      name: 'stub',
      description: 'stub',
    });

    const openField = ReactTestUtils.findRenderedDOMComponentWithClass(createVacancy, 'open-time-field');
    const closeField = ReactTestUtils.findRenderedDOMComponentWithClass(createVacancy, 'close-time-field');
    ReactTestUtils.Simulate.click(openField);
    ReactTestUtils.Simulate.keyDown(openField, { key: 'Enter', keyCode: 13, which: 13 });
    ReactTestUtils.Simulate.click(closeField);
    ReactTestUtils.Simulate.keyDown(closeField, { key: 'Enter', keyCode: 13, which: 13 });

    expect(createVacancy.state.formLoading).to.equal(false);
    createVacancy.handleSubmit(new Event('click'));
    expect(createVacancy.state.formLoading).to.equal(true);
    fetchMock.restore();
  });

  it('submit vacancy properly (success)', () => {
    fetchMock.post('*', { data: 'value' });
    fetchMock.get('*', {});
    Storage.set('user-data', companySession);
    const createVacancy = ReactTestUtils.renderIntoDocument(
      <CreateVacancy params={{ id: undefined }} user={{ data: { company: {} } }} />,
    );
    createVacancy.setState({
      name: 'stub',
      description: 'stub',
    });

    expect(createVacancy.state.formLoading).to.equal(false);
    createVacancy.handleSubmit(new Event('click'));
    expect(createVacancy.state.formLoading).to.equal(true);
    fetchMock.restore();
  });

  it('submit vacancy properly (loading)', () => {
    fetchMock.patch('*', 404);
    fetchMock.get('*', {});
    Storage.set('user-data', companySession);
    const createVacancy = ReactTestUtils.renderIntoDocument(
      <CreateVacancy params={{ id: 1 }} user={{ data: { company: {} } }} />,
    );
    createVacancy.setState({
      name: 'stub',
      description: 'stub',
    });

    const openField = ReactTestUtils.findRenderedDOMComponentWithClass(createVacancy, 'open-time-field');
    const closeField = ReactTestUtils.findRenderedDOMComponentWithClass(createVacancy, 'close-time-field');
    ReactTestUtils.Simulate.click(openField);
    ReactTestUtils.Simulate.keyDown(openField, { key: 'Enter', keyCode: 13, which: 13 });
    ReactTestUtils.Simulate.click(closeField);
    ReactTestUtils.Simulate.keyDown(closeField, { key: 'Enter', keyCode: 13, which: 13 });
    createVacancy.setOpenTime(moment());
    createVacancy.setCloseTime(moment());

    expect(createVacancy.state.formLoading).to.equal(false);
    createVacancy.handleSubmit(new Event('click'));
    expect(createVacancy.state.formLoading).to.equal(true);
    fetchMock.restore();
  });

  it('submit vacancy properly (success)', () => {
    fetchMock.patch('*', { data: 'value' });
    fetchMock.get('*', {});
    Storage.set('user-data', companySession);
    const createVacancy = ReactTestUtils.renderIntoDocument(
      <CreateVacancy params={{ id: 1 }} user={{ data: { company: {} } }} />,
    );
    createVacancy.setState({
      name: 'stub',
      description: 'stub',
    });

    expect(createVacancy.state.formLoading).to.equal(false);
    createVacancy.handleSubmit(new Event('click'));
    expect(createVacancy.state.formLoading).to.equal(true);
    fetchMock.restore();
  });
});
