/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import TopMenu from '../../components/TopMenu';

describe('TopMenu', () => {
  fetchMock.restore();
  fetchMock.get('*', { data: 'value' });

  const studentUser = {
    role: 'student',
    data: {
      url: 'http://localhost:8000/api/users/9/',
      username: 'muhammad.reza42',
      email: 'muhammad.reza42@ui.ac.id',
      is_staff: false,
      company: null,
      supervisor: null,
      student: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/9/',
          username: 'muhammad.reza42',
          email: 'muhammad.reza42@ui.ac.id',
          is_staff: false,
        },
        name: 'Muhammad R.',
        created: '2017-03-28T13:33:46.147241Z',
        updated: '2017-03-28T13:33:46.148248Z',
        npm: 1406543593,
        resume: null,
        phone_number: null,
        bookmarked_vacancies: [
          3,
        ],
        applied_vacancies: [
          3,
          1,
        ],
      },
    },
  };

  const companyUser = {
    role: 'company',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: true,
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8001/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535000Z',
        updated: '2017-03-28T07:30:10.535000Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        verified: true,
        logo: 'http://localhost:8001/files/company-logo/8a258a48-3bce-4873-b5d1-538b360d0059.png',
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
      supervisor: null,
      student: null,
    },
  };

  const adminUser1 = {
    role: 'admin',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: true,
      supervisor: null,
      student: null,
      company: {
        id: 3,
        user: {
          url: 'http://localhost:8001/api/users/8/',
          username: 'Tutuplapak',
          email: '',
          is_staff: false,
        },
        name: 'Tutuplapak',
        created: '2017-03-28T07:30:10.535000Z',
        updated: '2017-03-28T07:30:10.535000Z',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquet semper neque a fermentum. Duis ac tellus vitae augue iaculis ultrices. Curabitur commodo et neque nec feugiat. Morbi ac diam vel nunc commodo cursus. Phasellus nulla sapien, hendrerit vitae bibendum at, sollicitudin eu ante. Maecenas maximus, ante eu sollicitudin convallis, mauris nunc posuere risus, eu porttitor diam lacus vitae enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse at lectus a elit sollicitudin tempor. Nullam condimentum, justo nec tincidunt maximus, neque mi vulputate leo, sit amet lacinia massa ex eget sem. Duis ac erat facilisis, fringilla mauris in, consequat neque. In et neque consequat, vehicula magna at, efficitur ante. Mauris ac lacinia nibh.\r\n\r\nProin sagittis, lectus quis maximus varius, libero justo sollicitudin augue, non lacinia risus orci a enim. Curabitur iaculis enim quis ullamcorper commodo. Vivamus id nisi rhoncus, dignissim tellus quis, interdum est. Fusce sollicitudin eu libero ac feugiat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas semper posuere ex, sed accumsan libero iaculis faucibus. Fusce laoreet ac ligula ut consectetur. Donec tortor mauris, rutrum at sodales et, viverra in dolor. Sed bibendum elit et maximus volutpat. Phasellus justo ipsum, laoreet sit amet faucibus eu, ultricies suscipit mauris. Nullam aliquam libero eu ante ultrices mattis. Donec non justo hendrerit neque volutpat placerat. Ut euismod est nec sem mollis, sit amet porttitor massa rhoncus. Aenean id erat sit amet nunc ultrices scelerisque non in ipsum. Curabitur sollicitudin nulla id mi accumsan venenatis.',
        verified: true,
        logo: 'http://localhost:8001/files/company-logo/8a258a48-3bce-4873-b5d1-538b360d0059.png',
        address: 'Jl. Kebayoran Baru nomor 13, Jakarta Barat',
      },
    },
  };

  const adminUser2 = {
    role: 'admin',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      company: null,
      supervisor: null,
      student: null,
    },
  };

  const supervisorUser = {
    role: 'supervisor',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      company: null,
      supervisor: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/9/',
          username: 'muhammad.reza42',
          email: 'muhammad.reza42@ui.ac.id',
          is_staff: false,
        },
        name: 'Muhammad R.',
        created: '2017-03-28T13:33:46.147241Z',
        updated: '2017-03-28T13:33:46.148248Z',
        npm: 1406543593,
        resume: null,
        phone_number: null,
        bookmarked_vacancies: [
          3,
        ],
        applied_vacancies: [
          3,
          1,
        ],
      },
      student: null,
    },
  };

  const errorUser = {
    role: 'error',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      company: null,
      supervisor: null,
      student: null,
    },
  };

  it('renders for company without problem', () => {
    const topmenu = ReactTestUtils.renderIntoDocument(
      <TopMenu user={companyUser} />);
    expect(topmenu).to.exist;
  });

  it('renders for supervisor without problem', () => {
    const topmenu = ReactTestUtils.renderIntoDocument(
      <TopMenu user={supervisorUser}>
        <div> test </div>
      </TopMenu>);
    expect(topmenu).to.exist;
  });

  it('renders for admin without problem', () => {
    const topmenu = ReactTestUtils.renderIntoDocument(
      <TopMenu user={adminUser1}>
        <div> test </div>
      </TopMenu>);
    expect(topmenu).to.exist;
  });

  it('renders for student without problem', () => {
    const topmenu = ReactTestUtils.renderIntoDocument(
      <TopMenu user={studentUser}>
        <div> test </div>
      </TopMenu>);
    expect(topmenu).to.exist;
  });

  it('renders for error without problem', () => {
    const topmenu = ReactTestUtils.renderIntoDocument(
      <TopMenu user={errorUser}>
        <div> test </div>
      </TopMenu>);
    expect(topmenu).to.exist;
  });

  it('renders for user to logout without problem', () => {
    fetchMock.get('*', { data: 'value' });
    const topmenu = ReactTestUtils.renderIntoDocument(
      <TopMenu user={adminUser1}>
        <div> test </div>
      </TopMenu>);
    topmenu.logout(new Event('click'));
    expect(topmenu).to.exist;
  });

  it('changes the activeItem when item is clicked', () => {
    const topmenu = ReactTestUtils.renderIntoDocument(
      <TopMenu user={adminUser2}>
        <div> test </div>
      </TopMenu>);
    topmenu.handleItemClick(new Event('click'), 'undefined');
    expect(topmenu.state.activeItem).to.equal(undefined);
  });
});

