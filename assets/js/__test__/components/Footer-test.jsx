import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import Footer from '../../components/Footer';

describe('Footer', () => {

  it('renders without problem', () => {
    const footer = ReactTestUtils.renderIntoDocument(
      <Footer params={{ id: 1 }} />);
    expect(footer).to.exist;
  });
});
