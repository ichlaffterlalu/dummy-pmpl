/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import ApproveModal from '../../components/ApproveModal';
import Applicant from '../../components/Applicant';

describe('ApproveModal', () => {
  it('renders without problem', () => {
    const modalApproval = ReactTestUtils.renderIntoDocument(
      <ApproveModal updateStatus={() => {}} data={{ key: 'value', student: { resume: 'asdasd' } }} />);
    expect(modalApproval).to.exist;
  });

  it('open without problem', () => {
    fetchMock.get('*', { student: { resume: 'asdasd' } });
    const modalApproval = ReactTestUtils.renderIntoDocument(
      <ApproveModal updateStatus={() => {}} data={{ key: 'value', student: { resume: 'asdasd' }, status: Applicant.APPLICATION_STATUS.NEW }} />);

    const modal = ReactTestUtils.findRenderedDOMComponentWithTag(modalApproval, 'Button');
    ReactTestUtils.Simulate.click(modal);
    fetchMock.restore();
  });

  it('open resume without problem', () => {
    fetchMock.get('*', { student: { resume: 'asdasd' } });
    const modalApproval = ReactTestUtils.renderIntoDocument(
      <ApproveModal updateStatus={() => {}} data={{ key: 'value', student: { resume: 'asdasd' }, status: Applicant.APPLICATION_STATUS.NEW, show_transcript: true }} />);
    modalApproval.gotoStudentResume();
    fetchMock.restore();
  });

  it('open transcript without problem', () => {
    fetchMock.get('*', { student: { resume: 'asdasd' } });
    const modalApproval = ReactTestUtils.renderIntoDocument(
      <ApproveModal updateStatus={() => {}} data={{ key: 'value', student: { resume: 'asdasd' }, status: Applicant.APPLICATION_STATUS.NEW, show_transcript: true }} />);
    modalApproval.gotoStudentTranscript();
    fetchMock.restore();
  });

  it('close without problem', () => {
    fetchMock.get('*', { student: { resume: 'asdasd' } });
    fetchMock.patch('*', { student: { resume: 'asdasd' } });
    const modalApproval = ReactTestUtils.renderIntoDocument(
      <ApproveModal updateStatus={() => {}} data={{ key: 'value', student: { resume: 'asdasd' }, status: Applicant.APPLICATION_STATUS.NEW  }} />);

    modalApproval.handleClose();
    expect(modalApproval.state.modalOpen).to.equal(false);
    fetchMock.restore();
  });

  it('reject without problem', () => {
    fetchMock.get('*', { student: { resume: 'asdasd' } });
    fetchMock.patch('*', { });
    const modalApproval = ReactTestUtils.renderIntoDocument(
      <ApproveModal updateStatus={() => {}} data={{ key: 'value', student: { resume: 'asdasd' }, cover_letter: 'asdasd' }} />);
    modalApproval.modal = { open: () => {} };
    modalApproval.reject();
    modalApproval.rejectApplication();
    modalApproval.gotoLink('link random');
    expect(modalApproval.state.rejectLoading).to.equal(true);
    fetchMock.restore();
  });

  it('apply without problem', () => {
    fetchMock.get('*', { student: { resume: 'asdasd' } });
    fetchMock.patch('*', { });
    const modalApproval = ReactTestUtils.renderIntoDocument(
      <ApproveModal updateStatus={() => {}} data={{ key: 'value', student: { resume: 'asdasd', show_transcript: true  } }} />);
    modalApproval.modal = { open: () => {} };
    modalApproval.accept();
    modalApproval.acceptApplication();
    modalApproval.gotoLink('link random');
    expect(modalApproval.state.acceptLoading).to.equal(true);
    fetchMock.restore();
  });
});
