import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import VerifyAdminModal from '../../components/VerifyAdminModal';
import fetchMock from 'fetch-mock';
import Storage from '../../lib/Storage';

describe('VerifyAdminModal', () => {

  it('renders without problem', () => {
    const verifyModal = ReactTestUtils.renderIntoDocument(
      <VerifyAdminModal />,
    );
    expect(verifyModal).to.exist;
  });

  it('open without problem', () => {
     const verifyModal = ReactTestUtils.renderIntoDocument(
       <VerifyAdminModal id={4} />);

     const modal = ReactTestUtils.findRenderedDOMComponentWithTag(verifyModal, 'Button');
     ReactTestUtils.Simulate.click(modal);
     expect(verifyModal.state.modalOpen).to.equal(true);
   });

   it('close without problem', () => {
     const verifyModal = ReactTestUtils.renderIntoDocument(
       <VerifyAdminModal id={4} />);

     verifyModal.handleClose();
     expect(verifyModal.state.modalOpen).to.equal(false);
   });
});
