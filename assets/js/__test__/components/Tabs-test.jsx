import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import Tabs from '../../components/Tabs';

describe('ApplyModal', () => {
  it('renders without problem', () => {
    const tabs = ReactTestUtils.renderIntoDocument(
      <Tabs selected={1} children={[]} />,
    );
    expect(tabs).to.exist;
  });

  it('renders handle click properly', () => {
    const tabs = ReactTestUtils.renderIntoDocument(
      <Tabs selected={1} children={[]} />,
    );
    tabs.handleClick('stub', new Event('click'));
    expect(tabs.state.selected).to.equal('stub');
  });
});
