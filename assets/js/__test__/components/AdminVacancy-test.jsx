/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import AdminVacancy from '../../components/AdminVacancy';
import Storage from '../../lib/Storage';

describe('AdminVacancy', () => {
  const fetchMock = require('fetch-mock');
  const response = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  };

  const response2 = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: 'pictures',
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: false,
  };

  const supervisorUser = {
    role: 'supervisor',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      student: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/9/',
          username: 'muhammad.reza42',
          email: 'muhammad.reza42@ui.ac.id',
          is_staff: false,
        },
        name: 'Muhammad R.',
        created: '2017-03-28T13:33:46.147241Z',
        updated: '2017-03-28T13:33:46.148248Z',
        npm: 1406543593,
        resume: null,
        phone_number: null,
        birth_place: null,
        birth_date: null,
        major: null,
        batch: null,
        show_resume: false,
        bookmarked_vacancies: [
          3,
          2,
        ],
        applied_vacancies: [
          3,
          1,
        ],
      },
    },
  };

  it('renders for verified without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <AdminVacancy status={4} data={response} />);
    expect(lowongan).to.exist;
  });

  it('renders for unverified without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <AdminVacancy status={3} data={response2} />);
    expect(lowongan).to.exist;
  });

  it('generate button without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <AdminVacancy status={3} data={response2} />);
    expect(lowongan.generateButton()).to.exist;
  });
});
