/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import fetchMock from 'fetch-mock';
import Pagination from '../../components/Pagination';
import Logger from '../../lib/Logger';

describe('Pagination', () => {
  const response = {
    count: 4,
    next: 'next',
    previous: 'prev',
    results: [{
      close_time: '2019-03-28T05:55:42Z',
      company: {
        address: 'kebayoran baru',
        id: 1,
        logo: null,
        name: 'tutup lapak',
      },
      created: '2017-03-28T07:05:47.128672Z',
      description: 'Lorem ipsum dolbh.',
      id: 1,
      name: 'Software Engineer',
      open_time: '2017-03-28T05:55:38Z',
      updated: '2017-03-28T07:34:13.122093Z',
      verified: true,
    }, {
      close_time: '2019-03-28T05:55:42Z',
      company: {
        address: 'kebayoran baru',
        id: 2,
        logo: null,
        name: 'tutup lapak',
      },
      created: '2017-03-28T07:05:47.128672Z',
      description: 'Lorem ipsum dolbh.',
      id: 2,
      name: 'Software Engineer',
      open_time: '2017-03-28T05:55:38Z',
      updated: '2017-03-28T07:34:13.122093Z',
      verified: true,
    },
    ],
  };

  const response2 = {
    count: 4,
    next: null,
    previous: null,
    results: [{
      close_time: '2019-03-28T05:55:42Z',
      company: {
        address: 'kebayoran baru',
        id: 1,
        logo: null,
        name: 'tutup lapak',
      },
      created: '2017-03-28T07:05:47.128672Z',
      description: 'Lorem ipsum dolbh.',
      id: 1,
      name: 'Software Engineer',
      open_time: '2017-03-28T05:55:38Z',
      updated: '2017-03-28T07:34:13.122093Z',
      verified: true,
    }, {
      close_time: '2019-03-28T05:55:42Z',
      company: {
        address: 'kebayoran baru',
        id: 2,
        logo: null,
        name: 'tutup lapak',
      },
      created: '2017-03-28T07:05:47.128672Z',
      description: 'Lorem ipsum dolbh.',
      id: 2,
      name: 'Software Engineer',
      open_time: '2017-03-28T05:55:38Z',
      updated: '2017-03-28T07:34:13.122093Z',
      verified: true,
    },
    ],
  };

  it('renders without problem', () => {
    fetchMock.get('*', response);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    expect(pagination).to.exist;
    fetchMock.restore();
  });

  it('renders without problem when it is the first or last page', () => {
    fetchMock.get('*', response2);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    expect(pagination).to.exist;
    fetchMock.restore();
  });

  it('get items without problem', () => {
    fetchMock.get('*', response);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    pagination.getItemsData().then(() => {
      expect(JSON.stringify(pagination.state.items)).to.equal(JSON.stringify(response.items));
    });
    fetchMock.restore();
  });

  it('renders without problem when failed getting data', () => {
    fetchMock.get('*', 404);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    expect(pagination).to.exist;
    fetchMock.restore();
  });

  it('can go prev without problem', () => {
    fetchMock.restore();
    fetchMock.get('*', response);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    pagination.getItemsData().then(() => {
      const prev = ReactTestUtils.scryRenderedDOMComponentsWithTag(pagination, 'Menu.Item')[0];
      ReactTestUtils.Simulate.click(prev);
    });
    pagination.handlePrev();
    fetchMock.restore();
  });

  it('can refresh without problem', () => {
    fetchMock.get('*', response);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    pagination.getItemsData().then(() => {
      const next = ReactTestUtils.scryRenderedDOMComponentsWithTag(pagination, 'Menu.Item')[1];
      ReactTestUtils.Simulate.click(next);
    });
    pagination.refresh();
    fetchMock.restore();
  });

  it('can go next without problem', () => {
    fetchMock.get('*', response);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    pagination.getItemsData().then(() => {
      const next = ReactTestUtils.scryRenderedDOMComponentsWithTag(pagination, 'Menu.Item')[2];
      ReactTestUtils.Simulate.click(next);
    });
    pagination.handleNext();
    fetchMock.restore();
  });

  it('cannot go next or prev without problem', () => {
    fetchMock.get('*', response2);
    const pagination = ReactTestUtils.renderIntoDocument(
      <Pagination child={<div />} url="test" />);
    pagination.getItemsData().then(() => {
      const next = ReactTestUtils.scryRenderedDOMComponentsWithTag(pagination, 'Menu.Item')[2];
      ReactTestUtils.Simulate.click(next);
    });
    pagination.state.first = true;
    pagination.state.last = true;
    pagination.handleNext();
    pagination.handlePrev();
    fetchMock.restore();
  });
});

