/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import ModalAlert from '../../components/ModalAlert';

describe('ModalAlert', () => {
  it('renders without problem', () => {
    const modalAlert = ReactTestUtils.renderIntoDocument(<ModalAlert />);
    expect(modalAlert).to.exist;
  });

  it('renders with open state set to false', () => {
    const modalAlert = ReactTestUtils.renderIntoDocument(<ModalAlert />);
    expect(modalAlert.state.open).to.equal(false);
  });

  it('modal state should be true when opened', () => {
    const modalAlert = ReactTestUtils.renderIntoDocument(<ModalAlert />);
    modalAlert.open();
    expect(modalAlert.state.open).to.equal(true);
  });

  it('modal closed properly with open state should be false', () => {
    const modalAlert = ReactTestUtils.renderIntoDocument(<ModalAlert />);
    modalAlert.close();
    expect(modalAlert.state.open).to.equal(false);
  });
});
