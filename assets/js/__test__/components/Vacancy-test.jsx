/* eslint-disable no-unused-expressions */
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import Vacancy from '../../components/Vacancy';
import Storage from '../../lib/Storage';

describe('Vacancy', () => {
  const fetchMock = require('fetch-mock');
  const response = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  };

  const response2 = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: 'pictures',
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  };

  const studentUser = {
    role: 'company',
    data: {
      url: 'http://localhost:8001/api/users/8/',
      username: 'Tutuplapak',
      email: '',
      is_staff: false,
      student: {
        id: 3,
        user: {
          url: 'http://localhost:8000/api/users/9/',
          username: 'muhammad.reza42',
          email: 'muhammad.reza42@ui.ac.id',
          is_staff: false,
        },
        name: 'Muhammad R.',
        created: '2017-03-28T13:33:46.147241Z',
        updated: '2017-03-28T13:33:46.148248Z',
        npm: 1406543593,
        resume: null,
        phone_number: null,
        birth_place: null,
        birth_date: null,
        major: null,
        batch: null,
        show_resume: false,
        bookmarked_vacancies: [
          3,
          2,
        ],
        applied_vacancies: [
          3,
          1,
        ],
      },
    },
  };


  it('renders with null picture and apply button without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status={0} user={studentUser} data={response} />);
    expect(lowongan).to.exist;
  });

  it('renders with null picture and cancel button without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status={1} user={studentUser} data={response} />);
    expect(lowongan).to.exist;
  });

  it('renders with picture and apply button without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status={0} user={studentUser} data={response2} />);
    expect(lowongan).to.exist;
  });

  it('renders with picture and cancel button without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status={2} user={studentUser} data={response2} />);
    expect(lowongan).to.exist;
  });

  it('renders for accepted without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status={4} user={studentUser} data={response2} />);
    expect(lowongan).to.exist;
  });

  it('renders for rejected without problem', () => {
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status={3} user={studentUser} data={response2} />);
    expect(lowongan).to.exist;
  });

  it('bookmarks without problem', () => {
    fetchMock.post('*', response);
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status="Daftar" user={studentUser} data={response2} />);
    const response3 = { student: { id: 1, name: 2 } };
    expect(lowongan.props.data.id).to.equal(3);
    Storage.set('user-data', response3);
    expect(lowongan.bookmark()).to.be.undefined;
    fetchMock.restore();
  });

  it('cancel bookmarks without problem', () => {
    fetchMock.delete('*', { data: 'value' });
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status="Daftar" user={studentUser} data={response2} bookmarked={1} />);
    const response3 = { student: { id: 1, name: 2 } };
    lowongan.removeVacancyApplication();
    lowongan.openConfirmationModal();
    lowongan.updateStatus();
    expect(lowongan.props.data.id).to.equal(3);
    Storage.set('user-data', response3);
    expect(lowongan.bookmark()).to.be.undefined;
    fetchMock.restore();
  });

  it('cancel bookmarks with problem', () => {
    fetchMock.delete('*', 404);
    const lowongan = ReactTestUtils.renderIntoDocument(
      <Vacancy status="Daftar" user={studentUser} data={response2} bookmarked={1} />);
    const response3 = { student: { id: 1, name: 2 } };
    lowongan.removeVacancyApplication();
    expect(lowongan.props.data.id).to.equal(3);
    Storage.set('user-data', response3);
    lowongan.updateStatus('failed');
    expect(lowongan.bookmark()).to.be.undefined;
    fetchMock.restore();
  });
});
