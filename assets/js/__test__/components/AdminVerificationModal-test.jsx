import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import AdminVerificationModal from '../../components/AdminVerificationModal';

describe('AdminVerificationModal', () => {
  it('renders without problem', () => {
    const modalAdmin = ReactTestUtils.renderIntoDocument(
      <AdminVerificationModal />);
    expect(modalAdmin).to.exist;
  });

  it('close without problem', () => {
    const modalAdmin = ReactTestUtils.renderIntoDocument(
      <AdminVerificationModal />);
    modalAdmin.handleClose();
    expect(modalAdmin.state.modalOpen).to.equal(false);
  });

  it('open without problem', () => {
  const modalAdmin = ReactTestUtils.renderIntoDocument(
    <AdminVerificationModal />);
  modalAdmin.handleOpen();
  expect(modalAdmin.state.modalOpen).to.equal(true);
});

});


