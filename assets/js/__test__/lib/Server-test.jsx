/* eslint-disable */
import Server from './../../lib/Server';

(function (document) {
    var cookies = {};
    document.__defineGetter__('cookie', function () {
        var output = [];
        for (var cookieName in cookies) {
            output.push(cookieName + "=" + cookies[cookieName]);
        }
        return output.join(";");
    });
    document.__defineSetter__('cookie', function (s) {
        var indexOfSeparator = s.indexOf("=");
        var key = s.substr(0, indexOfSeparator);
        var value = s.substring(indexOfSeparator + 1);
        cookies[key] = value;
        return key + "=" + value;
    });
    document.clearCookies = function () {
        cookies = {};
    };
})(document);

describe('Server get test', () => {
  const fetchMock = require('fetch-mock');
  const response = {
    close_time: '2019-03-28T05:55:42Z',
    company: {
      address: 'kebayoran baru',
      id: 1,
      logo: null,
      name: 'tutup lapak',
    },
    created: '2017-03-28T07:05:47.128672Z',
    description: 'Lorem ipsum dolbh.',
    id: 3,
    name: 'Software Engineer',
    open_time: '2017-03-28T05:55:38Z',
    updated: '2017-03-28T07:34:13.122093Z',
    verified: true,
  };

  it('Check Server get right response', () => {
    fetchMock.get('*', response);
    Server.get('/test', true).then((data) => {
      expect(JSON.stringify(response)).to.equal(JSON.stringify(data));
    });
    Server.get('/test', false).then((data) => {
      expect(JSON.stringify(response)).to.equal(JSON.stringify(data));
    });
  });

  it('Check Server get right response', () => {
    fetchMock.get('*', { hello: 'not-world' });
    Server.get('/test').then((data) => {
      expect(JSON.stringify(response)).to.equal(JSON.stringify(data));
    });
  });

  it('Check Server post right response', () => {
    fetchMock.post('*', response);
    Server.post('/test').then((data) => {
      expect(JSON.stringify(response)).to.equal(JSON.stringify(data));
    });
  });

  it('Check Server delete right response', () => {
    fetchMock.delete('*', response);
    Server.delete('/test').then((data) => {
      expect(JSON.stringify(response)).to.equal(JSON.stringify(data));
    });
  });

  it('Check Server patch right response', () => {
    fetchMock.patch('*', response);
    Server.patch('/test').then((data) => {
      expect(JSON.stringify(response)).to.equal(JSON.stringify(data));
    });
  });

  it('Check helper methods response', () => {
    fetchMock.get('*', response);
    Server.request('/test','GET');
    Server.sendRequest('/test', 'GET', {}, true).then((data) => { expect(data).to.exist; });

    fetchMock.mock('*', 199);
    Server.sendRequest('/test', 'GET', {}, true).then((data) => { expect(data).to.exist; });

    fetchMock.mock('*', 204);
    Server.sendRequest('/test', 'GET', {}, true).then((data) => { expect(data).to.exist; });

    fetchMock.mock('*', 200);
    Server.sendRequest('/test', 'GET', {}, true).then((data) => { expect(data).to.exist; });
  });

  it('Check submit method', () => {
    fetchMock.get('*', {hue : "hue", hui : "hui"});
    Server.submit('/test', {hue : "hue", hui : "hui"}, 'GET', true).then((data) => { expect(data).to.exist; });
  });

  it('Check isloggedin method', () => {
    expect(Server.isLoggedIn()).to.not.exist;
  });

  it('Check getcookie method', () => {
    document.cookie = ';test=test;';
    expect(Server.getCookie('test')).to.exist;
  });
});
