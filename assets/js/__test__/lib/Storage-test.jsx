/* eslint-disable */
import Storage from './../../lib/Storage';

describe('Storage get key', () => {
  it('Check Storage get key', () => {
    expect(Storage.get('test')).to.be.not.exist;
  });

  it('Check Storage get key on exist key', () => {
    Storage.set('test', 'hue');
    expect(Storage.get('test')).to.equal('hue');
  });
});

describe('Storage set key', () => {
  it('Check Storage set key', () => {
    expect(Storage.set('test','hue')).to.be.not.exist;
  });

  it('Check Storage set key on exist key', () => {
    Storage.set('test', 'hue');
    expect(Storage.get('test')).to.equal('hue');
  });
});

describe('Storage clear key', () => {
  it('Check Storage clear key', () => {
    Storage.set('test', 'hue');
    expect(Storage.get('test')).to.equal('hue');
    expect(Storage.clear()).to.be.not.exist;
    expect(Storage.get('test')).to.be.not.exist;
  });
});


describe('Storage getUserData test', () => {
  const fetchMock = require('fetch-mock');

  it('Check Storage when there is no userdata', () => {
    fetchMock.get('*', 'hue');
    Storage.set('user-data', null); // clear to make sure storage empty
    expect(Storage.get('user-data')).to.equal(null);
    Storage.getUserData().then(r => {expect(r).to.equal('hue');}); // Storage call
  });

  it('Check Storage when there is already userdata', () => {
    Storage.set('user-data','hue'); // set to make sure storage not empty
    Storage.getUserData().then(r => {expect(r).to.equal('hue');});
  });
});
