/* eslint-disable no-unused-expressions */
import Logger from './../../lib/Logger';

describe('Logger.log success', () => {
  it('Logger sent log properly', () => {
    const val = Logger.log('test');
    expect(JSON.stringify(val) === JSON.stringify(['test'])).to.be.true;
  });
});

describe('Logger.log error', () => {
  it('Logger sent error properly', () => {
    const val = Logger.error('test');
    expect(JSON.stringify(val) === JSON.stringify(['test'])).to.be.true;
  });
});

describe('Logger.log warn', () => {
  it('Logger sent warn properly', () => {
    const val = Logger.warn('test');
    expect(JSON.stringify(val) === JSON.stringify(['test'])).to.be.true;
  });
});

