import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, Redirect } from 'react-router';
import Dashboard from './Dashboard';
import Login from './Login';
import VacancyPage from './VacancyPage';
import ProfilePage from './ProfilePage';
import CompanyProfile from './CompanyProfile';
import CreateVacancy from './CreateVacancy';
import Server from './lib/Server';
import Storage from './lib/Storage';
import ApplicantPage from './ApplicantPage';
import TranscriptPage from './TranscriptPage';
import AdminVacancyPage from './AdminVacancyPage';
import CompanyPage from './CompanyPage';
import SupervisorPage from './SupervisorPage';

export default class App extends React.Component {

  static getRole() {
    if (Server.isLoggedIn()) {
      const data = Storage.get('user-data');
      if (data.is_staff) {
        return 'admin';
      } else if (data.supervisor) {
        return 'supervisor';
      } else if (data.student) {
        return 'student';
      } else if (data.company) {
        return 'company';
      }
      return 'error';
    }
    return 'not-logged-in';
  }

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.handleAuth = this.handleAuth.bind(this);
    this.handleHome = this.handleHome.bind(this);
  }

  authorization = allowedRoles =>
    WrappedComponent => (
      /* eslint-disable no-multi-comp */
      class WithAuthorization extends React.Component {
        constructor(props) {
          super(props);
          this.state = {
            user: {
              role: App.getRole(),
              data: Storage.get('user-data'),
            },
          };
        }

        render() {
          const { role } = this.state.user;
          // const props = this.props;
          if (allowedRoles.includes(role)) {
            return <WrappedComponent {...this.props} {...this.state} />;
          }
          return <div> { browserHistory.push('/home') } </div>;
        }
      });

  handleAuth = (nextState, replace) => {
    if (!Server.isLoggedIn()) replace({ pathname: '/login' });
    Storage.getUserData();
  };

  handleHome= (nextState, replace, cb) => {
    if (Server.isLoggedIn()) {
      if (App.getRole() === 'student') {
        replace({ pathname: '/lowongan' });
        cb();
      } else if (App.getRole() === 'company') {
        replace({ pathname: '/lowongan' });
        cb();
      } else if (App.getRole() === 'admin') {
        replace({ pathname: '/perusahaan' });
        cb();
      } else if (App.getRole() === 'supervisor') {
        replace({ pathname: '/lowongan' });
        cb();
      }
    }
    replace({ pathname: '/login' }); cb();
  };

  render() {
    const staff = this.authorization(['admin']);
    const student = this.authorization(['admin', 'student']);
    const supervisor = this.authorization(['admin', 'supervisor']);
    const company = this.authorization(['admin', 'company']);
    const commonUser = this.authorization(['admin', 'student', 'company']);
    const grownups = this.authorization(['admin', 'company', 'supervisor']);
    const facultyMember = this.authorization(['admin', 'student', 'supervisor']);
    const all = this.authorization(['admin', 'company', 'supervisor', 'student']);

    return (
      <Router history={browserHistory}>
        <Route path="/login" component={Login} />
        <Route component={all(Dashboard)} onEnter={this.handleAuth}>
          <Route path="/transkrip/:id" component={commonUser(TranscriptPage)} />
          <Route path="/lowongan" component={all(VacancyPage)} />
          <Route path="/admin-lowongan" component={AdminVacancyPage} />
          <Route path="/buat-lowongan" component={company(CreateVacancy)} />
          <Route path="/buat-lowongan/:id" component={company(CreateVacancy)} />
          <Route path="/pelamar" component={company(ApplicantPage)} />
          <Route path="/profil-mahasiswa" component={student(ProfilePage)} own />
          <Route path="/profil-perusahaan" component={company(CompanyProfile)} />
          <Route path="/mahasiswa/:id" component={grownups(ProfilePage)} />
          <Route path="/perusahaan/:id" component={facultyMember(CompanyProfile)} />
          <Route path="/perusahaan" component={staff(CompanyPage)} />
          <Route path="/rekap" component={supervisor(SupervisorPage)} />
        </Route>
        <Route path="/home" onEnter={this.handleHome} />
        <Route path="/admin-vacancy" component={VacancyPage} />
        <Redirect from="*" to="/home" />
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('react-app'));
