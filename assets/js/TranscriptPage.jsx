import React from 'react';
import CourseList from './components/CourseList';
import Server from './lib/Server';

export default class TranscriptPage extends React.Component {
  static propTypes = {
    params: React.PropTypes.object.isRequired,
    user: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    /* istanbul ignore next */
    this.state = { text: 'Mohon Tunggu..' };
    const url = this.props.user.role === 'student' ? `/students/${this.props.params.id}/transcript/` : `/applications/${this.props.params.id}/transcript/`;
    Server.get(url).then(response => this.setState({ data: response }), () => this.setState({ text: 'Anda tidak berhak untuk melihat transkrip ini' }));
  }

  render() {
    return (
      this.state.data ? <CourseList data={this.state.data} /> : <h5 style={{ textAlign: 'center' }}> {this.state.text} </h5>
    );
  }
}
