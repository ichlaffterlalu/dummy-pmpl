# How To Deploy to Production Server

## Deployment

* Clone this repository
* Install docker, you can do this manually or using command `bash /provision/setup-docker.sh`
* Deploy using command `bash /provision/run-docker.sh DOCKERNAME APP_PORT SSH_PORT` for example `bash /provision/run-docker.sh staging 8000 8022`
* Profit :)

## HTTPS Server and Domain Name
* Add your domain name on the `ALLOWED_HOSTS` field on `kape/settings.py`
* Add these server blocks on yout NGINX setting (change `DOMAIN_NAME` and `APP_PORT`)
    ```
    server {
        listen 80;
        listen [::]:80;
        server_name your.DOMAIN_NAME.com;
        return 301 https://$server_name$request_uri;
    }

    server {
           listen 443 ssl default_server;
           listen [::]:443 ssl default_server;
           include snippets/ssl-your.DOMAIN_NAME.sslconfiguration.conf;
           server_name your.DOMAIN_NAME.com;
           location / {
                proxy_pass       http://localhost:APP_PORT;
                proxy_set_header Host      $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-Proto $scheme;
            }
    }
    ```

## Git push on the production server
* You can push directly to production git if SSH port is not blocked
* In the repo directory, run `git remote add production ssh://kape@DOMAIN_NAME:SSH_PORT/home/kape.git`
* To push to production server, run `git push production master` (default docker ssh password : yukcarikape)


# How To Build for Development Environment

## Requirements

* NodeJS + Npm Package Manager
* Python + Pip Package Manager
* PostgreSQL

## Installation

* `git clone https://gitlab.com/PPL2017csui/PPLA1.git kape`
* `cd kape`
* `npm install`
* `pip install -r requirements.txt`
* `psql` insert these queries:
    * `create user kape password 'kape';` to create user.
    * `create database kape;` to create database.
    * if you want to create another database user, please update the setting file `kape/settings.py`
* `python manage.py migrate` To migrate all the database

## Run Development Mode App

* `npm run webpack` to run webpack server. (if you don't want live update run `npm run build`, if you want the uglified+zipped+production ready version run `npm run build-production`)
* on a different terminal window, run `python manage.py runserver` to run API server

### Now go to localhost:8000 and you'll see the App running!

## Run Unit Test

* Frontend test, run `npm run webpack`
* Backend test, run `python manage.py test`
