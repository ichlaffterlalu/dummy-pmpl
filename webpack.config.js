const path = require('path');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');

module.exports = {
  context: __dirname,

  entry: './assets/js/index', // entry point of our app. assets/js/index.js should require other js modules and dependencies it needs

  output: {
    path: path.resolve('./assets/bundles/'),
    filename: '[name]-[hash].js',
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new BundleTracker({ filename: './webpack-stats.json' }),
    new webpack.OldWatchingPlugin(),
  ],

  module: {
    loaders: [
      { test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader', // to transform JSX into JS
        query:
        {
          presets: ['react', 'es2015', 'stage-1'],
        },
      },
    ],
    postLoaders: [{
      test: /\.jsx?$/,
      include: path.resolve('assets/js/'),
      exclude: [
        path.resolve('node_modules/'),
        path.resolve('assets/js/__test__/'),
      ],
      loader: 'istanbul-instrumenter-loader' }],
  },

  resolve: {
    modulesDirectories: ['node_modules', 'bower_components'],
    extensions: ['', '.js', '.jsx'],
  },
};
